package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {
    List<Usuarios> findByNome( String nome );
    Usuarios findByEmail( String email );
    Usuarios findByLogin( String login );
}
