package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    EspacosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Espacos salvar(Espacos espaco) {
        return repository.save(espaco);
    }

    @Transactional( rollbackFor = Exception.class )
    public Espacos editar(Integer id, Espacos espaco) {
        espaco.setId(id);
        return repository.save(espaco);
    }

    public List<Espacos> todasEspacos() {
        return (List<Espacos>) repository.findAll();
    }

    public Espacos espacoEspecifico( Integer id ) {
        Optional<Espacos> espaco =  repository.findById(id);
        return espaco.get();
    }

    public List<Espacos> espacoPorNome( String nome ){
        return repository.findByNome(nome);
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }
}
