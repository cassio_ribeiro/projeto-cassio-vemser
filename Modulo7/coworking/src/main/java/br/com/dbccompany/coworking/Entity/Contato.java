package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Util.Conversor;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table( name = "CONTATO")
public class Contato {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @ManyToOne(cascade =  CascadeType.ALL)
    @JoinColumn(name = "FK_ID_TIPO_CONTATO")
    private TipoContato tipo;

    @Column( name = "VALOR", nullable = false)
    private Double valor;

    @ManyToOne
    @JoinColumn(name = "ID_CLIENTE")
    private Clientes cliente;

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContato getTipo() {
        return tipo;
    }

    public void setTipo(TipoContato tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        String valorString = Double.toString(this.valor);
        return valorString;
    }

    public void setValor(String valor) {
        Double valorDouble = Conversor.conversorStringToDouble(valor);
        this.valor = valorDouble;
    }
}
