package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Service.ContatoService;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/contratacao")
public class ContratacaoController {
    @Autowired
    ContratacaoService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Contratacao adicionar(@RequestBody Contratacao contratacao ) {
        return service.salvar(contratacao);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Contratacao editar( @RequestBody Contratacao contratacao, @PathVariable Integer id ) {
        return service.editar( id, contratacao );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Contratacao> buscarTodosRegistros() {
        return service.todosContratacao();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Contratacao buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.contratacaoEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

    @PostMapping( value = "/orcamento" )
    @ResponseBody
    public Double orcamento(@PathVariable Integer espacoId, @PathVariable Integer contratacaoId){
        return service.valorContrato(espacoId, contratacaoId);
    }
}
