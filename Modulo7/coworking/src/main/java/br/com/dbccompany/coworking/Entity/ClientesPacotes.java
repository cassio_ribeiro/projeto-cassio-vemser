package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CLIENTES_X_PACOTES")
public class ClientesPacotes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_X_PACOTES_SEQ", sequenceName = "CLIENTES_X_PACOTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_X_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn( name = "FK_ID_CLIENTES")
    private Clientes cliente;

    @ManyToOne(cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_PACOTES")
    private Pacotes pacote;

    @Column( name = "QUANTIDADE" )
    private Integer quantidade;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
