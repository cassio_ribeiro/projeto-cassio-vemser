package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Repository.ClientesPacotesRepository;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.OneToMany;
import java.util.List;
import java.util.Optional;

@Service
public class ClientesPacotesService {

    @Autowired
    ClientesPacotesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public ClientesPacotes salvar(ClientesPacotes clientesPacotes) {
        return repository.save(clientesPacotes);
    }


    @Transactional( rollbackFor = Exception.class )
    public ClientesPacotes editar(Integer id, ClientesPacotes clientesPacotes) {
        clientesPacotes.setId(id);
        return repository.save(clientesPacotes);
    }

    public List<ClientesPacotes> todasClientesPacotes() {
        return (List<ClientesPacotes>) repository.findAll();
    }

    public ClientesPacotes clientesPacotesEspecifico( Integer id ) {
        Optional<ClientesPacotes> clientesPacotes =  repository.findById(id);
        return clientesPacotes.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }
}
