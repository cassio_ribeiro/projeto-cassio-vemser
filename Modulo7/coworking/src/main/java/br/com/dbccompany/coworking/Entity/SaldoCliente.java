package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
public class SaldoCliente {
    @EmbeddedId
    private SaldoClienteId id;

    @ManyToOne
    @MapsId("ID_CLIENTE")
    @JoinColumn( name = "ID_CLIENTE", nullable = false)
    private Clientes cliente;

    @ManyToOne
    @MapsId("ID_ESPACO")
    @JoinColumn( name = "ID_ESPACO", nullable = false)
    private Espacos espaco;

    @Enumerated(EnumType.STRING)
    @Column( name = "TIPO_CONTRATACAO", nullable = false)
    private TipoContratacao tipoContratacao;

    @Column( name = "QUANTIDADE", nullable = false )
    private Integer quantidade;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm" )
    @Column( name = "VENCIMENTO", nullable = false )
    private Date vencimento;

    public SaldoClienteId getId() {
        return id;
    }

    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
