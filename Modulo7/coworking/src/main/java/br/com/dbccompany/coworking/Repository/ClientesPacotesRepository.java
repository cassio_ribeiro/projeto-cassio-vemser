package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotes, Integer>{
}