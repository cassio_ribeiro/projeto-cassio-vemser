package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {

    @Autowired
    TipoContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public TipoContato salvar(TipoContato tipoContato) {
        return repository.save(tipoContato);
    }


    @Transactional( rollbackFor = Exception.class )
    public TipoContato editar(Integer id, TipoContato tipoContato) {
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }

    public List<TipoContato> todasTipoContato() {
        return (List<TipoContato>) repository.findAll();
    }

    public TipoContato tipoContatoEspecifico( Integer id ) {
        Optional<TipoContato> TipoContato =  repository.findById(id);
        return TipoContato.get();
    }

    public List<TipoContato> tipoContatoPorNome( String nome ){
        return repository.findByNome(nome);
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

}