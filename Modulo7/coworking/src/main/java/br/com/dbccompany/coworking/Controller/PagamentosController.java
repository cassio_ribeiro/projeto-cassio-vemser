package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/pagamentos")
public class PagamentosController {
    @Autowired
    PagamentosService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Pagamentos adicionar(@RequestBody Pagamentos pagamentos ) {
        return service.salvar(pagamentos);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Pagamentos editar( @RequestBody Pagamentos pagamentos, @PathVariable Integer id ) {
        return service.editar( id, pagamentos );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Pagamentos> buscarTodosRegistros() {
        return service.todasPagamentos();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Pagamentos buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.pagamentoEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

}