package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "PAGAMENTOS" )
public class Pagamentos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_CLIENTES_X_PACOTES", nullable = true)
    private ClientesPacotes clientesPacotes;

    @ManyToOne( cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_CONTRATACAO", nullable = false )
    private Contratacao contratacao;


    @Enumerated(EnumType.STRING)
    @Column( name =  "TIPO_PAGEMENTO" )
    private TipoPagamento tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotes getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotes clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
