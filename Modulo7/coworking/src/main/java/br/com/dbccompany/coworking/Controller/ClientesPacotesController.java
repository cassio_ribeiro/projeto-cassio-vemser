package br.com.dbccompany.coworking.Controller;


import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.ClientesPacotes;
import br.com.dbccompany.coworking.Service.AcessosService;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/clientesPacotes")
public class ClientesPacotesController {

    @Autowired
    private ClientesPacotesService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public ClientesPacotes adicionar(@RequestBody ClientesPacotes clientesPacotes ) {
        return service.salvar(clientesPacotes);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public ClientesPacotes editar( @RequestBody ClientesPacotes clientesPacotes, @PathVariable Integer id ) {
        return service.editar( id, clientesPacotes );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<ClientesPacotes> buscarTodosRegistros() {
        return service.todasClientesPacotes();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public ClientesPacotes buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.clientesPacotesEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

}
