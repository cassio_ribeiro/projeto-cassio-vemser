package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Repository.SaldoClienteRepositry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {
    @Autowired
    SaldoClienteRepositry repository;

    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return repository.save(saldoCliente);
    }


    @Transactional( rollbackFor = Exception.class )
    public SaldoCliente editar(SaldoClienteId id, SaldoCliente saldoCliente) {
        saldoCliente.setId(id);
        return repository.save(saldoCliente);
    }

    public List<SaldoCliente> todasSaldosClientes() {
        return (List<SaldoCliente>) repository.findAll();
    }

    public SaldoCliente saldoClienteeEspecifico( SaldoClienteId id ) {
        Optional<SaldoCliente> saldoCliente =  repository.findById(id);
        return saldoCliente.get();
    }
    @Transactional( rollbackFor = Exception.class )
    public SaldoClienteId deletar( SaldoClienteId id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return null;
        }
    }


}
