package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer> {
    List<TipoContato> findByNome( String nome );
}
