package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AcessosService {

    @Autowired
    AcessosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Acessos salvar(Acessos acesso) {
        return repository.save(acesso);
    }

    @Transactional( rollbackFor = Exception.class )
    public Acessos editar(Integer id, Acessos acesso) {
        acesso.setId(id);
        return repository.save(acesso);
    }

    public List<Acessos> todosAcessos() {
        return (List<Acessos>) repository.findAll();
    }

    public Acessos acessoEspecifica( Integer id ) {
        Optional<Acessos> acesso =  repository.findById(id);
        return acesso.get();

    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }



}
