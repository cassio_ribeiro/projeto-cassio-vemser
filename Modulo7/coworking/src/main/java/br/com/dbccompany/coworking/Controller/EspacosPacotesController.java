package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.EspacosPacotes;
import br.com.dbccompany.coworking.Service.EspacosPacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.EmptyStackException;
import java.util.List;

@Controller
@RequestMapping("api/EspacosPacotes")
public class EspacosPacotesController {

    @Autowired
    EspacosPacotesService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public EspacosPacotes adicionar(@RequestBody EspacosPacotes espacosPacotes ) {
        return service.salvar(espacosPacotes);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public EspacosPacotes editar(@RequestBody EspacosPacotes espacosPacotes, @PathVariable Integer id ) {
        return service.editar( id, espacosPacotes );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<EspacosPacotes> buscarTodosRegistros() {
        return service.todasEspacosPacotes();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public EspacosPacotes buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.espacosPacotesEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }
}
