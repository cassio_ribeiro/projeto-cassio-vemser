package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacosRepository extends CrudRepository<Espacos, Integer> {
    List<Espacos> findByNome( String nome );
}
