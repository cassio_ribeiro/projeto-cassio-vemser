package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Service.AcessosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/acessos" )
public class AcessosController {

    @Autowired
    private AcessosService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Acessos adicionar(@RequestBody Acessos acesso ) {
        return service.salvar(acesso);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Acessos editar( @RequestBody Acessos acesso, @PathVariable Integer id ) {
        return service.editar( id, acesso );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Acessos> buscarTodosRegistros() {
        return service.todosAcessos();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Acessos buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.acessoEspecifica( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }
}
