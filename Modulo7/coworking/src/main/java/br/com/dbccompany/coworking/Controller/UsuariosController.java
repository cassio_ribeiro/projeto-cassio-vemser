package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/usuarios")
public class UsuariosController {
    @Autowired
    UsuariosService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Usuarios adicionar(@RequestBody Usuarios usuarios ) {
        return service.salvar(usuarios);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Usuarios editar( @RequestBody Usuarios usuarios, @PathVariable Integer id ) {
        return service.editar( id, usuarios );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Usuarios> buscarTodosRegistros() {
        return service.todasUsuarios();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Usuarios buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.usuariosEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

    @GetMapping( value = "/get/{nome}" )
    @ResponseBody
    public List<Usuarios> buscarTodosRegistrosPorNome(String nome) {
        return service.usuariosPorNome(nome);
    }

    @GetMapping( value = "/get/{email}" )
    @ResponseBody
    public Usuarios buscarTodosRegistrosPorEmail(String email) {
        return service.usuariosPorEmail(email);
    }

    @GetMapping( value = "/get/{login}" )
    @ResponseBody
    public Usuarios buscarTodosRegistrosPorLogin(String login) {
        return service.usuariosPorLogin(login);
    }


}
