package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.security.krb5.internal.PAData;

import java.util.List;

@Controller
@RequestMapping("api/pacotes")
public class PacotesController {
    @Autowired
    PacotesService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Pacotes adicionar(@RequestBody Pacotes pacotes ) {
        return service.salvar(pacotes);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Pacotes editar( @RequestBody Pacotes pacotes, @PathVariable Integer id ) {
        return service.editar( id, pacotes );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Pacotes> buscarTodosRegistros() {
        return service.todasPacotes();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Pacotes buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.pacoteEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

    @GetMapping( value = "/get/{valor}" )
    @ResponseBody
    public List<Pacotes> buscarTodosRegistros(@PathVariable Double valor) {
        return service.pacotesPorValor(valor);
    }



}
