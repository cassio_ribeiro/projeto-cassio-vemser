package br.com.dbccompany.coworking.Util;

import java.math.BigInteger;
import java.security.MessageDigest;

public class Criptografia {

    public static  String criptografar( String senha ){
        String senhaCriptografada = "";
        MessageDigest md;

        try{
            md = MessageDigest.getInstance("MD5");
            BigInteger hash = new BigInteger( 1, md.digest(senha.getBytes()));
            senhaCriptografada = hash.toString(16);
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }

        return senhaCriptografada;
    }
}
