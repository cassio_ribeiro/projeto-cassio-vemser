package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Util.Conversor;
import org.graalvm.compiler.lir.alloc.lsra.LinearScan;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "PACOTES" )
public class Pacotes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue(generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @Column( name = "VALOR", nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "pacote")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote")
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        String valorString = Double.toString(this.valor);
        return valorString;
    }

    public void setValor(String valor) {
        Double valorDouble = Conversor.conversorStringToDouble(valor);
        this.valor = valorDouble;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
