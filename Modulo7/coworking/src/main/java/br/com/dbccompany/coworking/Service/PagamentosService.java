package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Pagamentos;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentosService {
    @Autowired
    PagamentosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Pagamentos salvar(Pagamentos pagamento) {
        return repository.save(pagamento);
    }


    @Transactional( rollbackFor = Exception.class )
    public Pagamentos editar(Integer id, Pagamentos pagamento) {
        pagamento.setId(id);
        return repository.save(pagamento);
    }

    public List<Pagamentos> todasPagamentos() {
        return (List<Pagamentos>) repository.findAll();
    }

    public Pagamentos pagamentoEspecifico( Integer id ) {
        Optional<Pagamentos> pagamento =  repository.findById(id);
        return pagamento.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }
}
