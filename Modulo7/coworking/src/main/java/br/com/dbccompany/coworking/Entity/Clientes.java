package br.com.dbccompany.coworking.Entity;

import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table( name = "CLIENTES")
public class Clientes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @Column( name = "NOME", nullable = false)
    private String nome;

    @Column( name = "CPF",  nullable = false, unique = true)
    private String cpf;

    @Column( name = "dataNascimento", nullable = false)
    private Date dataNascimento;

    @OneToMany(mappedBy = "cliente")
    private List<Contato> contatos;

    @OneToMany(mappedBy = "cliente")
    private List<ClientesPacotes> clientesPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "cliente")
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    public Clientes(Contato email, Contato telefone) {
        if(email.getTipo().getNome().equals("email")){
            this.contatos.add(email);
        }
        if(telefone.getTipo().getNome().equals("telefone")){
            this.contatos.add(telefone);
        }
        if(this.contatos.size() < 2){
            contatos.clear();
            return;
        }
    }


    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }


    public List<ClientesPacotes> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotes> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }
}
