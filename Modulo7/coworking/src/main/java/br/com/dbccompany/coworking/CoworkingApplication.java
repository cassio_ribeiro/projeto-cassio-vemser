package br.com.dbccompany.coworking;

import br.com.dbccompany.coworking.Entity.Usuarios;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoworkingApplication {

	public static void main(String[] args) {

		SpringApplication.run(CoworkingApplication.class, args);

	}
}
