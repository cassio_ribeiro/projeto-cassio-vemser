package br.com.dbccompany.coworking.Controller;
import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/espacos")
public class EspacosController {
    @Autowired
    EspacosService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Espacos adicionar(@RequestBody Espacos espacos ) {
        return service.salvar(espacos);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Espacos editar( @RequestBody Espacos espacos, @PathVariable Integer id ) {
        return service.editar( id, espacos );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Espacos> buscarTodosRegistros() {
        return service.todasEspacos();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Espacos buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.espacoEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

    @GetMapping( value = "/get/{nome}" )
    @ResponseBody
    public List<Espacos> buscarRegistroPorNome(@PathVariable  String nome ) {
        return service.espacoPorNome( nome );
    }




}
