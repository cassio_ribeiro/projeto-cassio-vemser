package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Service.ClientesPacotesService;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/clientes")
public class ClientesController {
    @Autowired
    ClientesService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Integer adicionar(@RequestBody Clientes cliente ) {
        return service.salvar(cliente);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Clientes editar( @RequestBody Clientes cliente, @PathVariable Integer id ) {
        return service.editar( id, cliente );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Clientes> buscarTodosRegistros() {
        return service.todosClientes();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Clientes buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.clienteEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

    @GetMapping( value = "/get/{nome}" )
    @ResponseBody
    public List<Clientes> buscarRegistroPorNome(@PathVariable  String nome ) {
        return service.clientePorNome( nome );
    }

    @GetMapping( value = "/get/{cpf}" )
    @ResponseBody
    public Clientes buscarRegistroPorCpf(@PathVariable  String cpf ) {
        return service.clientePorCpf(cpf);
    }

}
