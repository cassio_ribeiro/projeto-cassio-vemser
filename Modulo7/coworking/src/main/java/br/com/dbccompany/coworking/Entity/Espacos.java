package br.com.dbccompany.coworking.Entity;

import br.com.dbccompany.coworking.Util.Conversor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table( name = "ESPACOS")
public class Espacos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_SEQ", sequenceName = "ESPACOS_SEQ")
    @GeneratedValue(generator = "ESPACOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @Column( name = "NOME", nullable = false, unique = true)
    private String nome;

    @Column( name = "QTD_PESSOAS", nullable = false)
    private Integer qtdPessoas;

    @Column( name = "VALOR", nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "espaco")
    private List<EspacosPacotes> espacosPacotes = new ArrayList<>();

    @OneToMany( mappedBy = "espaco")
    private List<SaldoCliente> saldoClientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public String getValor() {
        String valorString = Double.toString(this.valor);
        return valorString;
    }

    public void setValor(String valor) {
        Double valorDouble = Conversor.conversorStringToDouble(valor);
        this.valor = valorDouble;
    }

    public List<EspacosPacotes> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotes> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<SaldoCliente> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoCliente> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }
}
