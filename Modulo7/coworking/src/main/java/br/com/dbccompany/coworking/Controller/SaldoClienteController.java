package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import br.com.dbccompany.coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/saldoCliente")
public class SaldoClienteController {
    @Autowired
    SaldoClienteService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public SaldoCliente adicionar(@RequestBody SaldoCliente saldoCliente ) {
        return service.salvar(saldoCliente);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public SaldoCliente editar(@RequestBody SaldoCliente saldoCliente, @PathVariable SaldoClienteId id) {
        return service.editar( id, saldoCliente );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<SaldoCliente> buscarTodosRegistros() {
        return service.todasSaldosClientes();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public SaldoCliente buscarRegistroPorId(@PathVariable SaldoClienteId id ) {
        return service.saldoClienteeEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public SaldoClienteId deletarRegistro(@PathVariable SaldoClienteId id){
        return service.deletar(id);
    }
}
