package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "ESPACOS_x_PACOTES" )
public class EspacosPacotes {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESPACOS_x_PACOTES_SEQ", sequenceName = "ESPACOS_x_PACOTES_SEQ")
    @GeneratedValue(generator = "ESPACOS_x_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_ESPACO")
    private Espacos espaco;

    @ManyToOne(cascade = CascadeType.MERGE )
    @JoinColumn( name = "FK_ID_PACOTES")
    private Pacotes pacote;

    @Enumerated(EnumType.STRING)
    @Column( name =  "TIPO_CONTRATACAO" )
    private TipoContratacao tipo;

    @Column( name = "QUANTIDADE", nullable = false)
    private Integer quantidade;

    @Column( name = "PRAZO", nullable = false)
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public TipoContratacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoContratacao tipo) {
        this.tipo = tipo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
