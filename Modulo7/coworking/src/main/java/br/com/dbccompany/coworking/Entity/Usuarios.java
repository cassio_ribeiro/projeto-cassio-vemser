package br.com.dbccompany.coworking.Entity;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "USUARIOS")
public class Usuarios {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "USUARIOS_SEQ", sequenceName = "USUARIOS_SEQ")
    @GeneratedValue(generator = "USUARIOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @Column( name = "NOME", nullable = false )
    private String nome;

    @Column( name = "EMAIL", nullable = false, unique = true )
    private String email;

    @Column( name = "LOGIN", nullable = false, unique = true  )
    private String login;

    @Column( name = "SENHA", nullable = false )
    private  String senha;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }


}
