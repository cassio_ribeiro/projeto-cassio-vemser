package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ACESSOS")
public class Acessos {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(
                    name = "ID_CLIENTES_SALDO_CLIENTE",
                    referencedColumnName = "ID_CLIENTE",
                    nullable = false
            ),
            @JoinColumn(
                    name = "ID_ESPACO_SALDO_CLIENTE",
                    referencedColumnName = "ID_ESPACO",
                    nullable = false
            )
    })
    private SaldoCliente saldoCliente;

    @Column(name = "IS_ENTRADA", nullable = false)
    private Boolean isEntrada;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm")
    @Column(name = "DATA", nullable = false)
    private Date data;

    @Column(name = "IS_EXCECAO", nullable = false)
    private Boolean isExcecao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoCliente getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoCliente saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}
