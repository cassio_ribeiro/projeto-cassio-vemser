package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Contato salvar(Contato contato) {
        return repository.save(contato);
    }


    @Transactional( rollbackFor = Exception.class )
    public Contato editar(Integer id, Contato contato) {
        contato.setId(id);
        return repository.save(contato);
    }

    public List<Contato> todasContatos() {
        return (List<Contato>) repository.findAll();
    }

    public Contato contatoEspecifico( Integer id ) {
        Optional<Contato> contato =  repository.findById(id);
        return contato.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }
}
