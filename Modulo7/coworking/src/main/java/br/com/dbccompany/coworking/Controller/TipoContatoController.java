package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.TipoContato;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/tipoContato")
public class TipoContatoController {
    @Autowired
    TipoContatoService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public TipoContato adicionar(@RequestBody TipoContato tipoContato ) {
        return service.salvar(tipoContato);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public TipoContato editar( @RequestBody TipoContato tipoContato, @PathVariable Integer id ) {
        return service.editar( id, tipoContato );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<TipoContato> buscarTodosRegistros() {
        return service.todasTipoContato();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public TipoContato buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.tipoContatoEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }

    @GetMapping( value = "/get/{nome}" )
    @ResponseBody
    public List<TipoContato> buscarTodosRegistrosPorNome(String nome) {
        return service.tipoContatoPorNome(nome);
    }



}
