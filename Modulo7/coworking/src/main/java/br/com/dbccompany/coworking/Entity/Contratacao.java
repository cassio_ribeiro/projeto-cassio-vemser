package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
@Table( name = "CONTRATACAO" )
public class Contratacao {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column( name = "ID" )
    private Integer id;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_ESPACOS"  )
    private Espacos espacos;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "FK_ID_CLIENTES"  )
    private Clientes cliente;

    @Enumerated(EnumType.STRING)
    @Column( name =  "TIPO_CONTRATCAO" )
    private TipoContratacao tipo;

    @Column( name = "QUANTIDADE", nullable = false)
    private Integer quantidade;

    @Column( name = "DESCONTO", nullable = true)
    private Double desconto;

    @Column( name = "PRAZO", nullable = false)
    private Integer prazo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Espacos getEspacos() {
        return espacos;
    }

    public void setEspacos(Espacos espacos) {
        this.espacos = espacos;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public TipoContratacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoContratacao tipo) {
        this.tipo = tipo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Double getDesconto() {
        return desconto;
    }

    public void setDesconto(Double desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
