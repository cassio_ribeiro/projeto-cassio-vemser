package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Contratacao;
import br.com.dbccompany.coworking.Entity.Espacos;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import br.com.dbccompany.coworking.Util.Conversor;
import jdk.jshell.execution.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {
    @Autowired
    ContratacaoRepository repository;

    @Autowired
    EspacosRepository repositoryEspaco;

    @Transactional( rollbackFor = Exception.class )
    public Contratacao salvar(Contratacao contrato) {
        return repository.save(contrato);
    }


    @Transactional( rollbackFor = Exception.class )
    public Contratacao editar(Integer id, Contratacao contrato) {
        contrato.setId(id);
        return repository.save(contrato);
    }

    public List<Contratacao> todosContratacao() {
        return (List<Contratacao>) repository.findAll();
    }

    public Contratacao contratacaoEspecifico( Integer id ) {
        Optional<Contratacao> contrato =  repository.findById(id);
        return contrato.get();
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }

    public Double valorContrato(@PathVariable Integer espacoId, @PathVariable Integer contratoId ) {
       String valor = repositoryEspaco.findById(espacoId).get().getValor();
       Integer quantidade = repository.findById(contratoId).get().getQuantidade();

       Double valorDouble = Conversor.conversorStringToDouble(valor);

       return valorDouble * quantidade;
    }
}
