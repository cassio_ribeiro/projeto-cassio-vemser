package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Contato;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("api/contato")
public class ContatoController {
    @Autowired
    ContatoService service;

    @PostMapping( value = "/add" )
    @ResponseBody
    public Contato adicionar(@RequestBody Contato contato ) {
        return service.salvar(contato);
    }

    @PutMapping( value = "/edit/{id}" )
    @ResponseBody
    public Contato editar( @RequestBody Contato contato, @PathVariable Integer id ) {
        return service.editar( id, contato );
    }

    @GetMapping( value = "/get" )
    @ResponseBody
    public List<Contato> buscarTodosRegistros() {
        return service.todasContatos();
    }

    @GetMapping( value = "/get/{id}" )
    @ResponseBody
    public Contato buscarRegistroPorId(@PathVariable  Integer id ) {
        return service.contatoEspecifico( id );
    }

    @DeleteMapping( value = "/delete/{id}")
    @ResponseBody
    public Integer deletarRegistro(@PathVariable  Integer id){
        return service.deletar(id);
    }
}
