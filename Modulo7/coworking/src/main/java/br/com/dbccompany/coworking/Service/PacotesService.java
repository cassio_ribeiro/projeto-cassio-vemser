package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Entity.Pacotes;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {

    @Autowired
    PacotesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Pacotes salvar(Pacotes pacote) {
        return repository.save(pacote);
    }


    @Transactional( rollbackFor = Exception.class )
    public Pacotes editar(Integer id, Pacotes pacote) {
        pacote.setId(id);
        return repository.save(pacote);
    }

    public List<Pacotes> todasPacotes() {
        return (List<Pacotes>) repository.findAll();
    }

    public Pacotes pacoteEspecifico( Integer id ) {
        Optional<Pacotes> pacote =  repository.findById(id);
        return pacote.get();
    }

    public List<Pacotes> pacotesPorValor( Double valor ){
        return repository.findByValor(valor);
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }
}
