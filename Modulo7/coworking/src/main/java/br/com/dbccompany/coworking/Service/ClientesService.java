package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.Acessos;
import br.com.dbccompany.coworking.Entity.Clientes;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Integer salvar(Clientes cliente) {
        if(!cliente.getContatos().isEmpty()){
            repository.save(cliente);
            return 1;
        }
        return 0;
    }


    @Transactional( rollbackFor = Exception.class )
    public Clientes editar(Integer id, Clientes cliente) {
        cliente.setId(id);
        return repository.save(cliente);
    }

    public List<Clientes> todosClientes() {
        return (List<Clientes>) repository.findAll();
    }

    public Clientes clienteEspecifico( Integer id ) {
        Optional<Clientes> cliente =  repository.findById(id);
        return cliente.get();
    }

    public List<Clientes> clientePorNome( String nome ){
        return repository.findByNome(nome);
    }

    public Clientes clientePorCpf ( String cpf ){
        return repository.findByCpf(cpf);
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }


}
