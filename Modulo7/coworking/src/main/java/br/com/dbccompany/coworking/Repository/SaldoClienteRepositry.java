package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoCliente;
import br.com.dbccompany.coworking.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClienteRepositry extends CrudRepository<SaldoCliente, SaldoClienteId> {


}
