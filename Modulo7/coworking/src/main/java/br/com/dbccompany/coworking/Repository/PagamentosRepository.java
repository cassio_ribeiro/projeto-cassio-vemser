package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.Pagamentos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentosRepository extends CrudRepository<Pagamentos, Integer> {

}
