package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Util.Criptografia;
import org.apache.commons.lang3.StringUtils;
import br.com.dbccompany.coworking.Entity.Usuarios;
import br.com.dbccompany.coworking.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuariosService {
    @Autowired
    UsuariosRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Usuarios salvar(Usuarios usuarios) {
        String senha = usuarios.getSenha();

        if( senha.length() >= 6 && StringUtils.isAlphanumeric(senha) ){
            usuarios.setSenha(Criptografia.criptografar(senha));
        }else{
            throw new RuntimeException("Por favor, a senha devera ter 6 digitos alfanumericos");
        }

        return repository.save(usuarios);
    }

    @Transactional( rollbackFor = Exception.class )
    public Usuarios editar(Integer id, Usuarios usuario) {
        usuario.setId(id);
        return repository.save(usuario);
    }

    public List<Usuarios> todasUsuarios() {
        return (List<Usuarios>) repository.findAll();
    }

    public Usuarios usuariosEspecifico( Integer id ) {
        Optional<Usuarios> usuarios =  repository.findById(id);
        return usuarios.get();
    }

    public List<Usuarios> usuariosPorNome( String nome ){
        return repository.findByNome(nome);
    }

    public Usuarios usuariosPorEmail ( String email ){
        return repository.findByEmail(email);
    }

    public Usuarios usuariosPorLogin ( String login ){
        return repository.findByLogin(login);
    }

    @Transactional( rollbackFor = Exception.class )
    public Integer deletar( Integer id ){
        try{
            repository.deleteById( id );
            return id;
        }catch (Exception e) {
            return 0;
        }
    }
}
