package br.com.dbccompany.bancodigital.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CIDADES")
public class Cidades {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CIDADES_SEQ", sequenceName = "CIDADES_SEQ")
    @GeneratedValue(generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CIDADES")
    private Integer id;

    @Column(name = "NOME")
    private String nome;

    @ManyToOne(cascade =  CascadeType.ALL)
    @JoinColumn(name = "FK_ID_ESTADOS")
    private Estados estado;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable(name = "clientes_x_cidades",
            joinColumns = { @JoinColumn(name = "id_cidade")},
            inverseJoinColumns = {@JoinColumn(name = "id_cliente")})
    private List<Clientes> clientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estados getEstado() {
        return estado;
    }

    public void setEstado(Estados estado) {
        this.estado = estado;
    }


}
