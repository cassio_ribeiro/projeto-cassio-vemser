package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.Banco;
import br.com.dbccompany.bancodigital.entity.Cidades;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CidadesRepository extends CrudRepository<Cidades, Integer > {
    List<Cidades> findAllByNome(String nome );
}
