package br.com.dbccompany.bancodigital.entity;




import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CONTAS")
public class Contas {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CONTAS_SEQ", sequenceName = "CONTAS_SEQ")
    @GeneratedValue(generator = "CONTAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTAS")
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_CODIGO_AGENCIA")
    private Agencias agencia;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_TIPO_CONTA")
    private TipoConta tipo;

    @Column(name = "NUMERO")
    private Integer numero;

    @Column(name = "SALDO")
    private Double saldo;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable(name = "clientes_x_contas",
            joinColumns = { @JoinColumn(name = "id_conta")},
            inverseJoinColumns = {@JoinColumn(name = "id_cliente")})
    private List<Clientes> clientes = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Agencias getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencias agencia) {
        this.agencia = agencia;
    }

    public TipoConta getTipo() {
        return tipo;
    }

    public void setTipo(TipoConta tipo) {
        this.tipo = tipo;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

}
