package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.Contas;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContasRepository extends CrudRepository <Contas, Integer> {
    Contas findByNumero( Integer numero );
}
