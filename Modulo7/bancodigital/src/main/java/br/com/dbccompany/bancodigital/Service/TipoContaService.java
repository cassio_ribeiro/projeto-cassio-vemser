package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Repository.TipoContaRepository;
import br.com.dbccompany.bancodigital.entity.TipoConta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContaService {

    @Autowired
    private TipoContaRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public TipoConta salvar(TipoConta tipoConta) {
        return repository.save(tipoConta);
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoConta editar(TipoConta tipoConta, Integer id) {
        tipoConta.setId(id);
        return repository.save(tipoConta);
    }

    public List<TipoConta> todosTiposContas() {
        return (List<TipoConta>) repository.findAll();
    }

    public TipoConta buscaPorId( Integer id ) {
        return repository.findById(id).get();
    }

}
