package br.com.dbccompany.bancodigital.entity;


import javax.persistence.*;

@Entity
@Table(name = "BANCO")
public class Banco {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "BANCO_SEQ", sequenceName = "BANCO_SEQ")
    @GeneratedValue(generator = "BANCO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "CODIGO")
    private Integer codigo;

    @Column(name = "NOME")
    private String nome;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
