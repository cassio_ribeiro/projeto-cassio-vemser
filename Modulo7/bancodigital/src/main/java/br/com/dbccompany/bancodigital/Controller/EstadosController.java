package br.com.dbccompany.bancodigital.Controller;


import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.entity.Cidades;
import br.com.dbccompany.bancodigital.entity.Estados;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/estados" )
public class EstadosController {

    @Autowired
    EstadosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Estados> todosEstados() {
        return service.todasEstados();
    }

    @GetMapping ( value = "/{id}")
    @ResponseBody
    public Estados estadosPorId( @PathVariable Integer id ){
        return service.estadoEspecifico(id);
    }

    @GetMapping ( value = "/todos/{nome}")
    @ResponseBody
    public List<Estados> estadosPorNome( @PathVariable String nome ){
        return service.estadoPorNome(nome);
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Estados novoEstado( @RequestBody Estados estado ) {
        return service.salvar(estado);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public  Estados editarEstado( @PathVariable Integer id, @RequestBody Estados estado ) {
        return service.editar(estado, id);
    }
}
