package br.com.dbccompany.bancodigital.entity;


import javax.persistence.*;

@Entity
@Table(name = "ESTADOS")
public class Estados {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ESTADOS_SEQ", sequenceName = "ESTADOS_SEQ")
    @GeneratedValue(generator = "ESTADOS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ESTADOS")
    private Integer id;

    @Column(name = "NOME")
    private String nome;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "FK_ID_PAISES")
    private Paises pais;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Paises getPais() {

        return pais;
    }

    public void setPais(Paises pais) {
        this.pais = pais;
    }
}
