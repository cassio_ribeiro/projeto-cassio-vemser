package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.Movimentacoes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovimentacoesRepository extends CrudRepository<Movimentacoes, Integer> {

}
