package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Repository.BancoRepository;
import br.com.dbccompany.bancodigital.entity.Banco;
import br.com.dbccompany.bancodigital.entity.Cidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BancoService {

    @Autowired
    private BancoRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Banco salvar( Banco bancod ) {
        return repository.save(bancod);
    }

    @Transactional ( rollbackFor = Exception.class )
    public Banco editar( Banco banco, Integer id ) {
        banco.setCodigo(id);
        return repository.save(banco);
    }

    public List<Banco> todosBancos() {
        return (List<Banco>) repository.findAll();
    }

    public Banco bancoEspecifico( Integer codigo ) {
        Optional<Banco> banco = repository.findById(codigo);
        return banco.get();
    }

    public List<Banco> bancoPorNome(String nome ) {
        return repository.findAllByNome(nome);
    }

}
