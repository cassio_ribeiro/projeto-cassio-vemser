package br.com.dbccompany.bancodigital.entity;

import javax.persistence.*;

@Entity
@Table(name = "TIPO_CONTA")
public class TipoConta {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue(generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_TIPO_CONTA")
    private Integer id;

    @Column(name = "DESCRICAO")
    private String descricao;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
