package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.entity.Banco;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/banco" )
public class BancoController {

    @Autowired
    BancoService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Banco> todosBancos() {
        return service.todosBancos();
    }

    @GetMapping ( value = "/todos/{id}")
    @ResponseBody
    public Banco bancoPorId( @PathVariable Integer id ){
        return service.bancoEspecifico(id);
    }

    @GetMapping ( value = "/todos/{nome}")
    @ResponseBody
    public List<Banco> bancoPorNome( @PathVariable String nome ){
        return service.bancoPorNome(nome);
    }


    @PostMapping( value = "/novo" )
    @ResponseBody
    public Banco novoBanco( @RequestBody Banco banco) {
        return service.salvar(banco);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public  Banco editarBanco( @PathVariable Integer id, @RequestBody Banco banco ) {
        return service.editar(banco, id);
    }




}
