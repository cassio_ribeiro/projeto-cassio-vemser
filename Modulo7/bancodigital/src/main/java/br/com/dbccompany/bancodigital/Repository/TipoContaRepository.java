package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.TipoConta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContaRepository extends CrudRepository<TipoConta, Integer > {
}
