package br.com.dbccompany.bancodigital.Controller;


import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.entity.Agencias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/agencias" )
public class AgenciasController {

    @Autowired
    AgenciasService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Agencias> todasAgencias() {
        return service.todasAgencias();
    }

    @GetMapping( value = "/todas/{id}" )
    @ResponseBody
    public Agencias agenciaPorId( @PathVariable Integer id ) {
        return service.agenciaEspecifica(id);
    }

    @GetMapping( value = "/todas/{nome}")
    @ResponseBody
    public List<Agencias> agenciaPornome( @PathVariable String nome) {
        return service.agenciaPorNome(nome);
    }

    @PostMapping ( value = "/nova" )
    @ResponseBody
    public Agencias salvar( @RequestBody Agencias agencia ) {
        return service.salvar(agencia);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Agencias editar( @PathVariable Integer id, @RequestBody Agencias agencia ) {
        return service.editar(id, agencia);
    }


}
