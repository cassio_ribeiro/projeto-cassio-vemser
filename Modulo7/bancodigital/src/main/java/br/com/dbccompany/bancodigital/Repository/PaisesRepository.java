package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.Paises;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaisesRepository extends CrudRepository<Paises, Integer> {

}