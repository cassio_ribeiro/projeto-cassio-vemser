package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Repository.PaisesRepository;
import br.com.dbccompany.bancodigital.entity.Paises;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Service
public class PaisesService {
    @Autowired
    private PaisesRepository repository;

    @Transactional( rollbackFor = Exception.class)
    public Paises salvar(Paises pais) {
        return repository.save(pais);
    }

    @Transactional(rollbackFor = Exception.class)
    public Paises editar(Paises pais, Integer id) {
        pais.setId(id);
        return repository.save(pais);
    }

    public List<Paises> todosPaises() {
        return (List<Paises>) repository.findAll();
    }

    public Paises buscaPorId(Integer id) {
        return repository.findById(id).get();
    }
}
