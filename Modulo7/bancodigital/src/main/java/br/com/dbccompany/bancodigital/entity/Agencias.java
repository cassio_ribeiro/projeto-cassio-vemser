package br.com.dbccompany.bancodigital.entity;


import javax.persistence.*;

@Entity
@Table(name = "AGENCIAS")
public class Agencias {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "AGENCIAS_SEQ", sequenceName = "AGENCIAS_SEQ")
    @GeneratedValue(generator = "AGENCIAS_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "CODIGO")
    private Integer codigo;

    @Column(name = "NOME")
    private String nome;


    @ManyToOne(cascade =  CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CIDADES")
    private Cidades cidade;

    @ManyToOne(cascade =  CascadeType.ALL)
    @JoinColumn(name = "FK_CODIGO_BANCO")
    private Banco banco;


    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public Cidades getCidade() {
        return cidade;
    }

    public void setCidade(Cidades cidade) {
        this.cidade = cidade;
    }
}
