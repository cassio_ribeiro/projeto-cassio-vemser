package br.com.dbccompany.bancodigital.entity;

public enum TipoMovimentacao {
    CREDITO, DEBITO
}
