package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.Estados;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstadosRepository extends CrudRepository <Estados, Integer> {

    List <Estados> findAllByNome( String nome );

}
