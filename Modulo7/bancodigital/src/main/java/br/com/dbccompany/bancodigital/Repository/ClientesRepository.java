package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {
    List<Clientes> findAllByNome( String nome );
    List<Clientes> findAllByCpf( String cpf );
}
