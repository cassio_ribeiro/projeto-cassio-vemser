package br.com.dbccompany.bancodigital.entity;


import javax.persistence.*;

@Entity
@Table(name = "MOVIMENTACOES")
public class Movimentacoes {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "PAISES_SEQ", sequenceName = "PAISES_SEQ")
    @GeneratedValue(generator = "PAISES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_MOVIMENTACAO")
    private Integer id;

    @Column(name = "VALOR")
    private Double valor;

    @Enumerated(EnumType.STRING)
    private TipoMovimentacao tipo;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "FK_ID_CONTA")
    private Contas conta;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public TipoMovimentacao getTipo() {
        return tipo;
    }

    public void setTipo(TipoMovimentacao tipo) {
        this.tipo = tipo;
    }

    public Contas getConta() {
        return conta;
    }

    public void setConta(Contas conta) {
        this.conta = conta;
    }


}
