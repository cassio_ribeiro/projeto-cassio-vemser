package br.com.dbccompany.bancodigital.Controller;


import br.com.dbccompany.bancodigital.Service.BancoService;
import br.com.dbccompany.bancodigital.Service.MovimentacoesService;
import br.com.dbccompany.bancodigital.entity.Banco;
import br.com.dbccompany.bancodigital.entity.Movimentacoes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/movimentacoes" )
public class MovimentacoesController {

    @Autowired
    MovimentacoesService service;

    @GetMapping( value = "/todas" )
    @ResponseBody
    public List<Movimentacoes> todasMov() {
        return service.todosMovimentacoes();
    }

    @GetMapping ( value = "/todas/{id}")
    @ResponseBody
    public Movimentacoes movPorId( @PathVariable Integer id ){
        return service.movEspecifico(id);
    }

    @PostMapping( value = "/nova" )
    @ResponseBody
    public Movimentacoes novoMov( @RequestBody Movimentacoes mov) {
        return service.salvar(mov);
    }

    @PutMapping( value = "/editar/{id}" )
    @ResponseBody
    public  Movimentacoes editarMov( @PathVariable Integer id, @RequestBody Movimentacoes mov ) {
        return service.editar(mov, id);
    }


}
