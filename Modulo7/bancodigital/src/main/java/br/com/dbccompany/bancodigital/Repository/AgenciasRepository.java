package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.entity.Agencias;
import br.com.dbccompany.bancodigital.entity.Banco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgenciasRepository extends CrudRepository<Agencias, Integer > {

    List<Agencias> findAllByNome( String nome );
}
