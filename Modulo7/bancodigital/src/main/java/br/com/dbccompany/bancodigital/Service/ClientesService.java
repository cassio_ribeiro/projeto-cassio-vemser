package br.com.dbccompany.bancodigital.Service;


import br.com.dbccompany.bancodigital.Repository.ClientesRepository;
import br.com.dbccompany.bancodigital.entity.Cidades;
import br.com.dbccompany.bancodigital.entity.Clientes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    ClientesRepository repository;

    @Transactional( rollbackFor = Exception.class )
    public Clientes salvar( Clientes cliente ) {
        return repository.save(cliente);
    }

    @Transactional ( rollbackFor = Exception.class )
    public Clientes editar(Clientes cliente, Integer id ) {
        cliente.setId(id);
        return repository.save(cliente);
    }

    public List<Clientes> todosClientes() {
        return (List<Clientes>) repository.findAll();
    }

    public Clientes clienteEspecifico( Integer id ) {
        Optional<Clientes> cliente = repository.findById(id);
        return cliente.get();
    }

    public List<Clientes> clientesPorNome( String nome ) {
        return (List<Clientes>) repository.findAllByNome(nome);
    }

    public List<Clientes> clientesPorCpf( String cpf ) {
        return (List<Clientes>) repository.findAllByNome(cpf);
    }
}
