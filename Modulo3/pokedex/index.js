/* eslint-disable no-use-before-define */
const pokeApi = new PokeApi();
const msgErro = document.getElementById( 'msgErro' );
const input = document.getElementById( 'input' );

function validacaoValorInput() {
  const valorInput = input.value;
  const seValido = ( valorInput > 0 && valorInput < 802 );
  // eslint-disable-next-line no-restricted-globals
  // event.preventDefault();
  if ( seValido ) {
    msgErro.innerHTML = '';
    const pokeNum = input.value;
    // eslint-disable-next-line no-use-before-define
    buscar( pokeNum );
  } else {
    msgErro.innerHTML = 'Insira um id valido';
    renderizacaoPokemon( null );
  }
}

input.addEventListener( 'blur', () => { validacaoValorInput() } );

async function buscar( id ) {
  console.log( id );
  const pokemonEspecifico = await pokeApi.buscar( id );
  const poke = new Pokemon( pokemonEspecifico );
  this.renderizacaoPokemon( poke );
}

// eslint-disable-next-line no-unused-vars
function renderizacaoPokemon( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const numero = dadosPokemon.querySelector( '.numero' );
  const nome = dadosPokemon.querySelector( '.nome' );
  const tipo = dadosPokemon.querySelector( '.tipo' );
  const peso = dadosPokemon.querySelector( '.peso' );
  const altura = dadosPokemon.querySelector( '.altura' );
  const estatisticas = dadosPokemon.querySelector( '.estatisticas' );
  const imgPokemon = dadosPokemon.querySelector( '.picture img' );

  numero.innerHTML = pokemon == null ? '' : `Numero: ${ pokemon.numero }`;
  estatisticas.innerHTML = pokemon == null ? '' : `Estatisticas: <br>${ pokemon.estatisticas.toString() }`;
  tipo.innerHTML = pokemon == null ? '' : `Tipo: ${ pokemon.tipos.toString() }`;
  nome.innerHTML = pokemon == null ? '' : `Nome: ${ pokemon.nome }`;
  imgPokemon.src = pokemon == null ? '' : pokemon.img;
  peso.innerHTML = pokemon == null ? '' : `Peso: ${ pokemon.peso }`;
  altura.innerHTML = pokemon == null ? '' : `Altura: ${ pokemon.altura }`;
}

function getRandomInt( min, max ) {
  // eslint-disable-next-line no-param-reassign
  min = Math.ceil( min );
  // eslint-disable-next-line no-param-reassign
  max = Math.floor( max );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}

const bigbluebutton = document.getElementById( 'bigbluebutton' );
// eslint-disable-next-line no-unused-vars

bigbluebutton.addEventListener( 'click', () => {
  const pokeNum = getRandomInt( 1, 802 );
  buscar( pokeNum );
  msgErro.innerHTML = '';
  input.value = '';
} );
