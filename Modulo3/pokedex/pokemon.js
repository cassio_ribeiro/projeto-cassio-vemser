class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.numero = obj.id;
    this.nome = obj.name;
    this.img = obj.sprites.front_default;
    this.tipos = obj.types.map( item => item.type.name );
    this.peso = obj.weight / 10;
    this.altura = obj.height / 10;
    this.estatisticas = obj.stats.map( item => `${ item.stat.name }: ${ item.base_stat }` ).join( '<br>' );
  }
}
