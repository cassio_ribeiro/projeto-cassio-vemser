import React, { Component } from 'react';
import './App.css';
import ListaEpisodios from './models/listaEpisodios';
import Links from './components/links';

class Home extends Component {
  constructor(props) {
    super(props);
    this.listaEpisodios = new ListaEpisodios();
    //this.sortear = this.sortear.bind( this )
    this.state = {
      episodio: this.listaEpisodios.episodiosAleatorios,
      deveExibirMensagem: false
    }

    /* this.listaSeries = new ListaSeries();
    console.log(this.listaSeries.invalidas());
    console.log(this.listaSeries.filtrarPorAno(2020));
    console.log(this.listaSeries.procuraPorNome('Winona Ryder'));
    console.log(this.listaSeries.mediaDeEpisodios());
    console.log(this.listaSeries.totalSalarios(2));
    console.log(this.listaSeries.queroGenero("Caos"));
    console.log(this.listaSeries.queroTitulo("The"));
    console.log(this.listaSeries.creditos(0));  */
  }

  render() {
    

    return (
      <div className="App">
        <Links />
        <h1>Home</h1>  
      </div>
    );
  }
}


export default Home;
