export default class Series {
    constructor( titulo, anoEstreia, diretor ,distribuidora, elenco, genero, numeroEpisodios, temporadas ){
        this.titulo = titulo;
        this.anoEstreia = anoEstreia;
        this.diretor = diretor;
        this.elenco = elenco;
        this.genero = genero;
        this.numeroEpisodios = numeroEpisodios;
        this.temporada = temporadas;
    }
}