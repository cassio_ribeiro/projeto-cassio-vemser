import React, { Component } from 'react';

export default class EpisodioUi extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        const { episodio } = this.props
        return (
            <React.Fragment>
                <h2>{episodio.nome}</h2>
                <img src={episodio.url} alt={episodio.nome}></img>
                <span>Assistido?? { episodio.assistido ? 'Sim' : 'Não'},{ episodio.qtdVezesAssistido } vez(es) </span>
                <span>{ episodio.nota }</span>
                <h4>Duração: { episodio.duracaoEmMin }</h4>
                <h4>Temporada/Episódio: { episodio.temporadaEpisodio }</h4>
            </React.Fragment>
        )
    }
}