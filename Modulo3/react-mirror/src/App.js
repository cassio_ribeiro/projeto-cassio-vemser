import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';


import Home from './Home.js';
import ReactMirror from './React-mirror';
import JsFlix from './Info-js-flix';
import ListaAvaliacoes from './components/ListaAvaliacoes'
import TelaDetalheEpisodio from './components/TelaDetalheEpisodios.js';
export default class App extends Component {

  render() {
    return (
      <Router>
        <Route exact path="/" component={ Home } />
        <Route exact path="/react-mirror" component={ ReactMirror } />
        <Route exact path="/info-js-flix" component={JsFlix} />
        <Route path="/avaliacoes" component={ ListaAvaliacoes } />
        <Route path="/episodio/:id" component={ TelaDetalheEpisodio } />
      </Router>
    );
  }
}
