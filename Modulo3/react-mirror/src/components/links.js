import React from 'react';
import { Link } from 'react-router-dom';
export default (props) =>
    <div>
        <Link to="/">Home</Link> /
        <Link to="/react-mirror">Mirror</Link> /
        <Link to="/info-js-flix">Info Flix</Link>
    </div>