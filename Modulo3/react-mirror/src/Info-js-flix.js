import React, { Component } from 'react';
import './App.css';
import ListaSeries from './models/ListaSeries';
import Links from './components/links';

class Home extends Component {
  constructor(props) {
    super(props);
  
    this.ListaSeries = new ListaSeries();
    console.log(this.ListaSeries.invalidas());
    console.log(this.ListaSeries.filtrarPorAno(2020));
    console.log(this.ListaSeries.procuraPorNome('Winona Ryder'));
    console.log(this.ListaSeries.mediaDeEpisodios());
    console.log(this.ListaSeries.totalSalarios(2));
    console.log(this.ListaSeries.queroGenero("Caos"));
    console.log(this.ListaSeries.queroTitulo("The"));
    console.log(this.ListaSeries.creditos(0)); 
  }

  render() {
    

    return (
      <div className="App">
          <Links />

          <h1>Flix</h1>
      </div>
    );
  }
}

export default Home;
