import React, { Component } from 'react';
import Api from '..//Api';
import { Link } from 'react-router-dom';
import Header from '../componentes/header/header';
import axios from 'axios';
export default class ListaClientes extends Component {
  constructor() {
    super()
    this.api = new Api();
    this.state = {
      clientes: []
    }
  }

  componentDidMount() {
    axios.get('http://localhost:1337/clientes', {
      headers: {
        'Authorization': 'banco-vemser-api-fake'
      }
    })
      .then((res) => {
        this.setState({
          clientes: res.data.clientes
        });
        console.log(res.data)
      }).catch((err) => {
        console.log(err);
      })
  }

  pesquisarCliente = evt => {
    let nome = evt.target.value;
    return this.api.getClientes()
      .then(value => this.setState({ clientes: value.data.clientes.filter(cl => cl.nome.includes(nome)) }))
      .catch((err) => {
        console.log(err)
      })
  }

  render() {
    const { clientes } = this.state;
    return (
      <React.Fragment>
        <Header />

        <h1>Clientes</h1>
        <input class="form-control text-center" type="text" id="pesquisa" onBlur={this.pesquisarCliente} placeholder="Pesquise um cliente" />


        {
          clientes.map(cliente => {
            return <tr>
              <th scope="row">{cliente.id}</th>
              <th><Link to={{ pathname: `/clientes/${cliente.id}` }}>{cliente.nome}</Link></th>
            </tr>
          })
        }





      </React.Fragment>
    )
  }
}
