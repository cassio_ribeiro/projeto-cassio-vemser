import React, { Component } from 'react';
import * as axios from 'axios';
import Header from '../componentes/header/header';
import AgendaUi from '../models/AgenciaUi'

export default class Agencias extends Component {
    constructor(props) {
        super(props)
        this.state = {
            agencias: [],
            busca: " "
        }
        this.atualizaInput = this.atualizaInput.bind(this)
    }

    componentWillMount() {
        axios.get('http://localhost:1337/agencias', {
            headers: {
                'Authorization': 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({ agencias: res.data.agencias },
            )
        })
    }

    atualizaInput(e) {
        const { value } = e.target
        this.setState({
            busca: value
        })
    }

    render() {
        const { agencias, busca, enviar } = this.state
        const agencia = agencias.filter(agencia => agencia.nome.includes(busca))

        return (
            <React.Fragment>
                <Header />

                <div>
                    Buscar<input placeholder="Nome da Agencia" onChange={this.atualizaInput}></input>
                </div>

                <AgendaUi agencias={agencias} key={0} busca={busca} />

            </React.Fragment>

        )

    }
}