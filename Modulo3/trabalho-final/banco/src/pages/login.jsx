import React, { Component } from 'react';
import './css/login.css'
import Header from '../componentes/header/header';
import Input from '../componentes/input/input';
import Botao from '../componentes/botao/botao';
import axios from 'axios';
import { Redirect } from 'react-router-dom'
class Login extends Component {

    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { username: null, password: null,};
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.username = e.target.username.value;
        this.password = e.target.password.value;
        axios.post('http://localhost:1337/login', { email: this.username, senha: this.password })
            .then((res) => {
                console.log(res.data.status);
            }).catch((err) => {
                console.log(err);
            })
    }
    render() {
         return (
            <div className="App">
                <Header />
                <div className="container login">
                    <div className="row justify-content-center">
                        <form onSubmit={this.handleSubmit} className="col-4 border rounded border-secondary bg-secondary text-white" >
                            <Input id="username" name="username" className="form-control" type="email" label="Email" placeholder="Informe seu email" />

                            <Input id="password" name="password" className="form-control" type="password" label="Senha" placeholder="Informe sua senha" />

                            <Botao className="btn btn-primary" text="Entrar" />
                        </form>
                        
                    </div>
                </div> 

              
            </div>
        );
    }
}

export default Login;











