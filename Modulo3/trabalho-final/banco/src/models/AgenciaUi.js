import React, { Component } from 'react'


export default class AgenciaUi extends Component {

    constructor(props){
        super(props)
    }


    render(){
        const { agencias,busca} = this.props;
            let agencia = agencias.filter(agencia => agencia.nome.includes(busca))
            return (
                agencia.map((agencia) => 
                    <React.Fragment key={agencia.id} >
                        <div className="agencias-tela"> 
                            <h2>Agencia: 000{agencia.codigo} </h2> 
                            <h3>Nome de Agencia: {agencia.nome}</h3>
                            <h4> Endereço: </h4>
                            <p>Logradouro: {agencia.endereco.logradouro}</p>
                            <p>Numero {agencia.endereco.numero}</p>
                            <p>Bairro: {agencia.endereco.bairro}</p>
                            <p>Cidade: {agencia.endereco.cidade}</p>
                            <p>Uf: {agencia.endereco.uf}</p>
                        </div> 
                    </React.Fragment>
                        ) 
            )

        
    }
}