import React, { Component } from 'react';
import './input.css'


export default class Header extends Component {

    render() {
        const { name ,className, type, placeholder, label} = this.props
        return (
            <div className="form-group">
                <label>{label}</label>
                <input name={name} type={type} className={className} placeholder={placeholder} />
            </div>
        )
    }
}