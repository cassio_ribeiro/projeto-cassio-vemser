import React, { Component } from 'react';
import './botao.css'


export default class Header extends Component {

    render() {
        const {className, text} = this.props
        return (
            <button className={className} id="botao" >{text}</button>
        )
    }
}

// "btn btn-primary"