import axios from 'axios';

const token = "banco-vemser-api-fake";
const setToken = token => { localStorage.setItem( 'token', token) }
const getToken = () => localStorage.getItem('token');
const deslogar = () => {
    localStorage.removeItem('token')
}

export default class Api {

    constructor() {
      this.request = axios.create({
        baseURL: "http://localhost:1337"
      })
      
      this.request.interceptors.request.use( 
        config => {
            const token = getToken();
            if (token) {
            config.headers.Authorization = token;
            }
            return config;
        })
      }
    getAgencias = () => this.request.get('/agencias', { headers: { 'Authorization': getToken() } } );
  
    getClientes = () => this.request.get('/clientes', { headers: { 'Authorization': getToken() } } );
  
    getCliente = (id) => this.request.get(`/cliente/${id}`, { headers: { 'Authorization': getToken() } } );
  
    getAgencia = (id) => this.request.get(`/agencia/${id}`, { headers: { Authorization: getToken() } } );
  
    getTiposConta = () => this.request.get('/tipoContas', { headers: { 'Authorization': getToken() } } );
  
    getTipoConta = (id) => this.request.get(`/tiposConta/${id}`, { headers: { 'Authorization': getToken() } } );
  
    getContaClientes = () => this.request.get('/conta/clientes/', { headers: { 'Authorization': getToken() } } );
  
    getContaCliente = (id) => this.request.get(`/conta/cliente/${id}`, { headers: { 'Authorization': getToken() } } );

    login = (email, password) => this.request.post("/login", { "email": email, "senha":password });

}