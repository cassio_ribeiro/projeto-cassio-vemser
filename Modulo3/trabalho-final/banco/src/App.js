import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import Login from './pages/Login';
import Agencias from './pages/Agencias';
import IsAuthenticated from './Auth';
import Clientes from './pages/Clientes'

const PrivateRoute = ( { component: Component, ...rest } ) => (
  <Route { ...rest } render={ props => (
    IsAuthenticated() ? 
      ( <Component { ...props }/> ) :
      ( <Redirect to={ { pathname: '/agencias', state: { from: props.location } } }/> )
  )}/>
)

export default class App extends Component {


  render() {
    return (
      <React.Fragment>
        <Router>

          <Route exact path="/" component={Login} />
          <Route exact path="/agencias" component={Agencias} />
          <Route exact path="/clientes" component={Clientes} />
        
        </Router>
      </React.Fragment>

    );
  }
}
