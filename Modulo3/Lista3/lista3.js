function cardapioIFood( veggie = true, comLactose = true ) {
    let cardapio = [
      'enroladinho de salsicha',
      'cuca de uva'
    ]
     
    if ( comLactose ) {
      cardapio.push( 'pastel de queijo' )
    }
    //console.log(cardapio);
    
   cardapio = [...cardapio, 'pastel de carne', 'empada de legumes marabijosa'];
    
    if ( veggie ) {
      // TODO: remover alimentos com carne (é obrigatório usar splice!)
        cardapio.splice( cardapio.indexOf( 'enroladinho de salsicha' ), 1)
        cardapio.splice( cardapio.indexOf( 'pastel de carne' ), 1 )
    }  
    
    let resultado = cardapio.map(alimento => alimento.toLocaleUpperCase());

    /*let resultado = cardapio
                    .filter(alimento => alimento === 'pastel de carne')
                    .map(alimento => alimento.toLocaleUpperCase());
    */
    return resultado;
    
  }

  console.log(cardapioIFood()); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]