import React, { Component, Fragment } from 'react';
import '../css/header.css';
import '../css/grid.css';
import '../css/box.css';
import '../css/reset.css';
import Logo from '../img/logo-dbc-topo.png';


export default class Header extends Component {
    render() {
        
        return (
            <React.Fragment>
               <header class="main-header">
            <nav class="container clearfix">
                <a class="logo" href="index.html" title="Voltar à home">
                    <img src={Logo} alt="DBC Company" />
                </a>

                <label class="mobile-menu" for="mobile-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
                <input id="mobile-menu" type="checkbox" />

                <ul class="clearfix">
                    <li>
                        <a href="index.html">Home</a>
                    </li>
                    <li>
                        <a href="sobre-nos.html">Sobre nós</a>
                    </li>
                    <li>
                        <a href="servicos.html">Serviços</a>
                    </li>
                    <li>
                        <a href="contato.html">Contato</a>
                    </li>
                </ul>
            </nav>
        </header>

            </React.Fragment>
        )
    }
}
