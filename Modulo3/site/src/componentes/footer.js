import React, { Component, Fragment } from 'react';
import '../css/footer.css';
import '../css/grid.css';
import '../css/box.css';
import '../css/reset.css';
import Logo from '../img/logo-dbc-topo.png';


export default class Header extends Component {
    render() {

        return (
            <React.Fragment>
                <footer class="main-footer">
                    <div class="container">
                        <nav>
                            <ul>
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li>
                                    <a href="#">Sobre nós</a>
                                </li>
                                <li>
                                    <a href="#">Serviços</a>
                                </li>
                                <li>
                                    <a href="#">Contato</a>
                                </li>
                            </ul>
                        </nav>
                        <p>
                            &copy; Copyright DBC Company - 2019
                </p>
                    </div>
                </footer>
            </React.Fragment>
        )
    }
}
