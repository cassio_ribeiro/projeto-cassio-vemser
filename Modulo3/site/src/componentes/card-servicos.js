import React from 'react';
import '../css/banner.css';
import '../css/buttons.css';
import '../css/grid.css';
import '../css/reset.css';


export default (props) =>

    <div class={props.className}>
        <article class="box">
            <div>
            <img src={require(`../img/${props.imagem}`)} alt=""/>
            </div>
            {props.titulo}
            {props.children}
                <a class="button button-blue" href="coisa.html">{props.textButton}</a>
        </article>
    </div>