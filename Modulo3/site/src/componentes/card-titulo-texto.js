import React from 'react';
import '../css/banner.css';
import '../css/buttons.css';


export default (props) =>

    <article className={props.className}>
        {props.titulo}
        {props.children}
    </article>

