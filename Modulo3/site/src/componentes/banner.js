import React from 'react';
import '../css/banner.css';
import '../css/buttons.css';



export default ( props ) =>

    <div className='main-banner'>
        <article>
            {props.children}
        </article>    
    </div>