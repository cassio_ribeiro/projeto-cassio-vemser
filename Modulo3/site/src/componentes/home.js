import React, { Component } from 'react';
import '../css/grid.css';

import Header from './header';
import Footer from './footer';
import Banner from './banner';
import CardServicos from './card-servicos';
import CardTituloTexto from './card-titulo-texto';
export default class Home extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {

        return (
            <React.Fragment>
                <Header />
                <Banner>
                    <h1>Vem ser DBC</h1>
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus.
                        </p>
                    <a class="button button-big button-outline" href="index.html">Saiba mais</a>
                </Banner>

                <section className="container">
                    <div className="row">
                        <CardTituloTexto className="col col-12 col-md-7" titulo={<h2>Titulo</h2>} >
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                        </p>
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                        </p>
                        </CardTituloTexto>
                        <CardTituloTexto className="col col-12 col-md-5" titulo={<h3>Titulo</h3>} >
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                        </p>
                        </CardTituloTexto>
                    </div>
                </section>
                <section className="container">
                    <div className="row">
                        <CardServicos className="col col-12 col-md-6 col-lg-3" imagem="Linhas-de-Serviço-Agil.png" titulo={<h4>Título</h4>} textButton="Saiba mais">
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                            </p>
                        </CardServicos>
                    </div>

                </section>

                <Footer />
            </React.Fragment>
        )
    }
}
