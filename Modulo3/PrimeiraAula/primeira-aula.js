//console.log("Cheguei", "Aquiii");

var nomeVar = "valor";
let nomeLet = "valorLet";
const nomeConst = "nomeConst";



const nomeConst2 = {
    nome: "Cassio",
    idade: 26
};

Object.freeze(nomeConst2);

nomeConst2.nome = "Cassio Ribeiro";
nomeConst2.idade = "18";
//console.log(nomeConst2);

function nomeDaFuncao(){
    //nomeVar = "valor";
    let nomeLet = "valorLet2";
}

/*
function somar(){
    return 1 + 1;
}
*/

function somar(valor1, valor2 = 1){
    console.log(valor1 + valor2);   
}

//somar(1.2, 1);

function ondeMoro(cidade){
    //console.log("Eu moro em "+cidade+". E não é legal!!");
    console.log(`Eu moro em ${cidade}. E não é legal!! ${30 + 1} dias `);
}

//ondeMoro("Charqueadas");

function fruteira(){
    let texto = "Banana"
                +"\n"
                +"Ameixa"
                +"\n"
                +"Goiabada"
                +"\n"
                +"Pessego"
                +"\n";

    let newTexto = ` 
                Banana 
                Ameixa
                Goiabada
                Pessego
                `;    
                    
    console.log(texto);
    console.log(newTexto);                
}


//fruteira();

let eu = {
    nome: "Cássio",
    idade: 26,
    altura: 1.68 
};


function quemSou(pessoa){
    console.log(`Meu nome é ${pessoa.nome}, minha idade é ${pessoa.idade} e minha altura é ${pessoa.altura}`);
}

//quemSou(eu);


let funcaoSomar = function(a, b){
    return a + b;
}

let add = funcaoSomar;
let resultado = add(4, 5);
//console.log(resultado);

const { nome:n, idade:i } = eu;

//console.log(n, i);

const array = [1,3,4,5];
const [ n1, ,n3, n2, n4 = 18] = array;

//console.log(n1,n2,n3,n4);

function testarPessoa({nome, idade, altura}){
    console.log(nome, idade, altura);
}

//testarPessoa(eu);

let a1 = 42;
let b1 = 15;
console.log(a1, b1);
 

//inverter lets
[a1, b1] = [b1, a1];

console.log(a1, b1);



