import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './Home'
import ListaItens from './ListaItens'

export default class App extends Component {

  render() {
    return (
      <Router>
         <Route exact path="/" component={ Home } />
         <Route exact path="/listaItens" component = {ListaItens} />

      </Router>
    );
  }
}
