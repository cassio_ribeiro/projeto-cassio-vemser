import React from 'react'
import '../css/botao.css'
import Input from './input.js'


export default (props) =>

    <div class="form-group">
        <button type="submit" className={props.class} >{props.value}</button>
    </div>
