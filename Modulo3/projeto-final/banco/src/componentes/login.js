import React, { Component } from 'react'
import '../css/login.css'
import Input from './input.js'
import Botao from './botao.js'
import axios from 'axios';

export default class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: null,
            senha: null,
            token: null
        };
    }

    componentDidMount(e) {
        /* e.preventDefault(); */
        const { email, senha, token } = this.state
        axios.post('http://localhost:1337/login', { email: email, senha: senha })
            .then((res) => {
                console.log(res.data);
                this.setState({
                    token: "banco-vemser-api-fake"
                });
                console.log("sucesso")
            }).catch((err) => {
                console.log(err);
            })
    }


    render() {
        return (
            <React.Fragment>
                <div class="container login-container">
                    <div class="row">
                        <div class="col-md-6 offset-3 login-form-1 bg-secondary">
                            <h3>Bem Vindo!</h3>

                            <Input placeholder="Login" />
                            <Input placeholder="Senha" />

                            <Botao class="btnSubmit" value="Login" />
                            <div class="form-group">
                                <a href="home.html" class="btnForgetPwd">Forget Password?</a>
                            </div>

                        </div>
                    </div>
                </div>

            </React.Fragment>
        )

    }
}


