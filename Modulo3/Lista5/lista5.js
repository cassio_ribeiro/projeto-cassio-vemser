
const baseUrl = 'https://pokeapi.co/api/v2/pokemon/';


const input = document.getElementById('input');
const containerPokemon = document.getElementById('pokemon');
const button = document.getElementById('button');

let pokeNum;// = input.value; 
let pokemon;
let card;
let valorInput = input.value;


function requerimentoInfoPokemon(url, numero) {
  fetch(url + numero)
    .then(data => data.json())
    .then(data => {
      pokemon = data;
    })
    .catch(err => console.log(err));
}

function createCard() {
  
  card = `
  <div class="container">
  <div class="row">
    <div class="card" >
    <img src="${pokemon.sprites.front_default}" alt="Sprite of ${pokemon.name}" class="imagemPokemon">
      <div class="card-body">
        <h5 class="card-title">Nome:${pokemon.name}</h5>
        <h5 class="card-title">Numero:${pokemon.id}</h5>
        <h4 class="card-title">Tipo:${pokemon.types.map(item => item.type.name).toString()}</h4>
          <h4 class="skill">Estatisticas: ${pokemon.stats.map(item => ' ' + item.stat.name +": "+item.stat.base_stat).toString()}
          </h4>
          <h4 class="weight">Weight: ${pokemon.weight / 10}kg</h4>
          <h4 class="height">Height: ${pokemon.height / 10}m</h4>
      </div>
    </div>
  </div>

</div>
  `;



  
  return card;




}

// Função que faz a chamada das principais funções e inicia o app
function startApp(pokeNum) {
  requerimentoInfoPokemon(baseUrl, pokeNum);

  setTimeout(function () {

    containerPokemon.innerHTML = createCard();

  }, 1000);
}

// Add Events --------------------------------------------
input.addEventListener('blur', () => {
  if (valorInput <= 0 || valorInput > 802) {
    alert("informe um id valido: 1 a 802");
  } else {
    pokeNum = input.value.toLowerCase();
    startApp(pokeNum);
  }

});

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

button.addEventListener('click', () => {
  
  
  pokeNum = getRandomInt(1, 802);

  startApp(pokeNum);
});

