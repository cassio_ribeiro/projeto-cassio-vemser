import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import './img/logo-dbc-topo.png';
import './App.css';
import './css/header.css';
import './css/header.css';
import './css/reset.css';
import './css/about-us.css';
import './css/grid.css';
import './css/banner.css';
import './css/general.css';
import './css/footer.css';
import './css/work.css';
import './css/contact.css';
import './css/buttons.css';
import './css/box.css';



import Footer from './componentes/footer';
import Home from './componentes/home';
import SobreNos from './componentes/sobre-nos';
import Servicos from './componentes/servicos';
import Contato from './componentes/contato';

export default class App extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor( props ) {
    super( props );
 
  }

  


  render(){
    return (
      <div className="App">
        
          
        
        <Router>
          <Route path="/" exact component = { Home } />
          <Route path="/sobrenos" exact component = { SobreNos } />
          <Route path="/servicos" exact component = { Servicos } />
          <Route path="/contato" exact component = { Contato } />
        </Router>
        
        <footer className="main-footer">
         
          <Footer />
        </footer>
      </div>
    );
  }
  
}



