import React, { Component } from 'react';
import Card from './card-servicos';
import Header from './header';
export default class Servicos extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        return (
            <React.Fragment>
                <Header />
                <div className="container work">
                    <div className="row">
                        <Card imagem="Linhas-de-Serviço-DBC_Software-Builder.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Sustain.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Software-Builder.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Software-Builder.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Sustain.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Software-Builder.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <article class="col col-12">
                            <h3>Título</h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus quae similique consectetur, expedita praesentium provident quisquam aperiam repellat, dicta, blanditiis quia quaerat aliquam saepe suscipit molestias placeat perspiciatis sint ipsa.
                            </p>
                            <a class="button button-big button-blue">Saiba mais</a>
                        </article>
                    </div>

                </div>

            </React.Fragment>
        )
    }
}