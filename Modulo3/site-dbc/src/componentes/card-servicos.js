import React, { Component } from 'react';
export default class Servicos extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        const { imagem, titulo, texto } = this.props
        return (
            <React.Fragment>
               <div class="col col-12 col-md-6 col-lg-4">
                    <article class="box">
                        <div>
                            <img src={require(`../img/${imagem}`)}></img>
                        </div>
                        <h2>{titulo}</h2>
                        <p>
                            {texto}
                        </p>
                    </article>
                </div>
            </React.Fragment>
        )
    }
}