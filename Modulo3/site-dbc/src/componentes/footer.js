import React, { Component } from 'react';


export default class Footer extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        //const { episodio } = this.props
        return (
            <React.Fragment>
                <div class="container">
                    <nav>
                        <ul>
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a href="sobre-nos.html">Sobre nós</a>
                            </li>
                            <li>
                                <a href="servicos.html">Serviços</a>
                            </li>
                            <li>
                                <a href="contato.html">Contato</a>
                            </li>
                        </ul>
                    </nav>
                    <p>
                        &copy; Copyright DBC Company - 2019
                </p>
                </div>


            </React.Fragment>
        )
    }
}