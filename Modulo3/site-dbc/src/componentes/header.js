import React, { Component } from 'react';
import Logo from '../img/logo-dbc-topo.png';
import { Link, BrowserRouter as Router } from 'react-router-dom';

export default class Header extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {

        return (
            <Router>
                <header className="main-header">
                    <nav className="container clearfix">
                        <Link className="logo" to="/" title="Voltar à home">
                            <img src={Logo} alt="DBC Company"></img>
                        </Link>

                        <label className="mobile-menu" for="mobile-menu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </label>
                        <input id="mobile-menu" type="checkbox"></input>

                        <ul class="clearfix">
                            <li>
                                <Link to="/">Home</Link>
                            </li>
                            <li>
                                <Link to="/sobrenos">Sobre nós</Link>
                            </li>
                            <li>
                                <Link to="/servicos">Serviços</Link>
                            </li>
                            <li>
                                <Link to="/contato">Contato</Link>
                            </li>
                        </ul>
                    </nav>
                </header>


            </Router>
        )
    }
}