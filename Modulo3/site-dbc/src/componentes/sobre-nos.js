import React, { Component } from 'react';
import Card from './card-sobre-nos';
import Header from './header';
export default class Home extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {

        return (
            <React.Fragment>
                <Header />
                <div className="container about-us">
                    <div className="row">
                        <Card imagem="Linhas-de-Serviço-Agil.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Smartsourcing.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Software-Builder.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                        <Card imagem="Linhas-de-Serviço-DBC_Sustain.png" titulo="titulo" texto="Lorem, ipsum dolor sit amet consectetur adipisicing elit. Commodi repellendus libero dolores a eveniet. Optio reiciendis consequatur ipsum facilis, error consequuntur culpa porro mollitia sint veniam neque accusantium magni eaque." />
                    </div>
                </div>
            </React.Fragment>
        )
    }
}