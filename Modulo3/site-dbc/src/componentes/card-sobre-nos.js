import React, { Component } from 'react';
import '../css/about-us.css';

export default class CardSobreNos extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        const { imagem, titulo, texto } = this.props
        return (
            <React.Fragment>
               <div class="row">
                <article class="col col-12 about">
                    <img src={require(`../img/${imagem}`)}></img>
                    <h2>{titulo}</h2>
                    <p>
                        {texto}
                    </p>
                </article>
            </div>
            </React.Fragment>
        )
    }
}