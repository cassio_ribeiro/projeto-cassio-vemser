import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
export default class Links extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        const { rota, texto } = this.props
        return (
            <Route>

                <li>
                    <Link to = {rota}> {texto} </Link>
                </li>
                
            </Route>
        )
    }
}