import React, { Component } from 'react';
import Agil from '../img/Linhas-de-Serviço-Agil.png'
import '../css/contact.css';
import Header from './header';

export default class Contato extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        //const { episodio } = this.props
        return (
            <React.Fragment>
                <Header />
                <div className="container work">
                    <div className="row">
                        <div className="col col-12 col-md-6">
                            <h2>Título</h2>
                            <p>
                                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fugiat molestiae harum ex delectus. Placeat doloribus illum aliquid, similique a vel perspiciatis modi porro explicabo quae. Dicta consequatur esse explicabo minus.
                                </p>
                            <form className="clearfix">
                                <input class="field" type="text" placeholder="Nome"></input>
                                <input class="field" type="text" placeholder="E-mail"></input>
                                <input class="field" type="text" placeholder="Assunto"></input>
                                <textarea class="field" placeholder="Mensagem"></textarea>
                                <input class="button button-green button-right" type="submit" value="Enviar"></input>
                            </form>

                        </div>
                    </div>
                    <div className="map-container">
                        <iframe className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760855!2d-51.17087028474702!3d-30.01619288189262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576870098547!5m2!1spt-BR!2sbr" allowfullscreen="" title="afa"></iframe>
                    </div>
                </div>



            </React.Fragment>
        )
    }
}