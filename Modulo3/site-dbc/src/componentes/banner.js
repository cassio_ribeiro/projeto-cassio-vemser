import React, { Component } from 'react';


export default class Banner extends Component {
    //https://reactjs.org/docs/components-and-props.html
    render() {
        //const { episodio } = this.props
        return (
            <React.Fragment>
                <section class="main-banner">
                    <article>
                        <h1>Vem ser DBC</h1>
                        <p>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis, voluptatem! Totam aliquid tenetur quia laudantium accusamus amet nostrum! Dolorem inventore vitae, in quis culpa amet impedit molestiae sint laborum repellendus.
                        </p>
                        <a class="button button-big button-outline" href="aa">Saiba mais</a>
                    </article>
                </section>
                <div class="container">
                    <div class="row">
                        <article class="col col-12 col-md-7">
                            <h2>Título</h2>
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                    </p>
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                    </p>
                        </article>
                        <article class="col col-12 col-md-5">
                            <h3>Título</h3>
                            <p>
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati ratione excepturi vitae, nostrum corporis libero fuga recusandae, velit adipisci quidem officia eum, iste maiores eos similique. Reiciendis dignissimos repudiandae sint.
                    </p>
                        </article>
                    </div>
                </div>
            </React.Fragment>
                )
            }
}