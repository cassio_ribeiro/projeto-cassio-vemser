function criarSanduiche( pao, recheio, queijo, salada ){
    console.log(`Seu sanduiche ${pao} com recheio ${recheio} e queijo ${queijo} com ${salada}`); 
}

const ingredientes = ['3 queijos', 'Frango', 'Cheddar', 'Tomate e alface'];

//chamado em 40 lugares

//criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor=> console.log(valor));
}

//receberValoresIndefinidos(1,2,3,4,5,6);

//console.log([..."Cassio"]);

// Window of Document

let inputTeste = document.getElementById('campoTeste');

inputTeste.addEventListener('blur', () => {
    alert("Obrigado");
})