

function calcularCirculo({raio, tipoDeCalculo:tipo}){
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);
}

let circulo = {
    raio: 3,
    tipoDeCalculo: "A"
}

//console.log(calcularCirculo(circulo));


/*function naoBissexto(ano){
    return !((ano % 4 == 0) && (ano % 100 != 0)) || (ano % 400 == 0);
}*/

//arrow function
let naoBissexto = ano => !((ano % 4 == 0) && (ano % 100 != 0)) || (ano % 400 == 0);


//console.log(naoBissexto(2016)); //retorna false;
//console.log(naoBissexto(2015)); //retorna true;

function somarPares(vet){
    let soma = 0;
        for(let i = 0; i < vet.length; i ++){
        if(i % 2 == 0){
            soma += vet[i];
        }
    }
        return soma;
}

//console.log(somarPares([1,2,3,4,5]));


/*function adicionar(valor1){
   return function adicionar2(valor2) {
        return valor1 + valor2;
    }
    
}*/

//arrow function
let adicionar = valor1 => valor2 => valor1 + valor2;
//console.log(adicionar(5)(2));

/*
const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor,20));
console.log(is_divisivel(divisor,11));
console.log(is_divisivel(divisor,9));
*/

//currying
const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12));






