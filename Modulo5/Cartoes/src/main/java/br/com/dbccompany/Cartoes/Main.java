package br.com.dbccompany.Cartoes;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.Credenciador_X_LojaEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;
import br.com.dbccompany.Cartoes.Entity.Util;

public class Main {

	public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("Loja do Internacional");
			
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Cassio");
			
			CredenciadorEntity creden = new CredenciadorEntity();
			creden.setNome("Credito facil");
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("Visa");
			bandeira.setTaxa(0.03);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Vendedor1");
			emissor.setTaxa(0.5);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setChip(1);
			cartao.setVencimento("15/12");
			cartao.setBandeira(bandeira);
			cartao.setCliente(cliente);
			cartao.setEmissor(emissor);
			
			LancamentoEntity lanc = new LancamentoEntity();
			lanc.setDataCompra("10/10/15");
			lanc.setDescricao("camiseta do inter");
			lanc.setCartao(cartao);
			lanc.setEmissor(emissor);
			lanc.setLoja(loja);
			lanc.setValor(150.15);
			
			List<CredenciadorEntity> creditos = new ArrayList<>();
			creditos.add(creden);
			
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			
			Credenciador_X_LojaEntity credXLoja = new Credenciador_X_LojaEntity();
			credXLoja.setCredenciador(creditos);
			credXLoja.setLoja(lojas);
			credXLoja.setTaxa(0.5);
			
			Credenciador_X_LojaEntity credXLoja2 = new Credenciador_X_LojaEntity();
			credXLoja2.setCredenciador(creditos);
			credXLoja2.setLoja(lojas);
			credXLoja2.setTaxa(0.6);
			
			
			session.save(loja);
			session.save(cliente);
			session.save(creden);
			session.save(bandeira);
			session.save(emissor);
			session.save(cartao);
			session.save(lanc);
			session.save(credXLoja);
			session.save(credXLoja2);
			//Usando o session.get apenas para mostrar que a consulta foi no banco;
			LancamentoEntity lancConsulta = (LancamentoEntity) session.get(LancamentoEntity.class, 1);
			Credenciador_X_LojaEntity credConsulta = (Credenciador_X_LojaEntity) session.get(Credenciador_X_LojaEntity.class, 1);
			EmissorEntity emissorConsulta = (EmissorEntity) session.get(EmissorEntity.class, 1);
			BandeiraEntity bandConsulta = (BandeiraEntity) session.get(BandeiraEntity.class, 1);
			
			Util.MontaLucros(lancConsulta.getValor(),
							credConsulta.getTaxa(), 
							emissorConsulta.getTaxa(), 
							bandConsulta.getTaxa());
			
			
			transaction.commit();
		} catch (Exception e) {
			if( transaction != null ) {
				transaction.rollback();
			}
			System.exit(1);
		}finally {
			System.exit(0);
		}
	}
	
}
