package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CARTAO")
public class CartaoEntity {
	
	@Id
	@SequenceGenerator( allocationSize = 1, name = "CARTAO_SEQ", sequenceName  = "CARTAO_SEQ")
	@GeneratedValue( generator = "CARTAO_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_CLIENTE",nullable = false)
	private Integer id;
	
	@Column( name = "CHIP")
	private Integer chip;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_CLIENTE", nullable = false)
	private ClienteEntity cliente;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_BANDEIRA", nullable = false)
	private BandeiraEntity bandeira;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_EMISSOR", nullable = false)
	private EmissorEntity emissor;
	
	@Column(name = "VENCIMENTO")
	private String vencimento;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "ID_LANCAMENTO")
	private List<LancamentoEntity> lacamentos = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getChip() {
		return chip;
	}

	public void setChip(Integer chip) {
		this.chip = chip;
	}

	public ClienteEntity getCliente() {
		return cliente;
	}

	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}

	public BandeiraEntity getBandeira() {
		return bandeira;
	}

	public void setBandeira(BandeiraEntity bandeira) {
		this.bandeira = bandeira;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}

	public String getVencimento() {
		return vencimento;
	}

	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}

	public List<LancamentoEntity> getLacamentos() {
		return lacamentos;
	}

	public void setLacamentos(List<LancamentoEntity> lacamentos) {
		this.lacamentos = lacamentos;
	}
	
	
	
	
	
	

}
