package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LANCAMENTO")
public class LancamentoEntity {
	@Id
	@SequenceGenerator( allocationSize = 1, name = "LANCAMENTO_SEQ", sequenceName  = "LANCAMENTO_SEQ")
	@GeneratedValue( generator = "LANCAMENTO_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_LANCAMENTO", nullable = false)
	private Integer id;
	

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_LOJA", nullable = false)
	private LojaEntity loja;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_CARTAO", nullable = false)
	private CartaoEntity cartao;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FK_ID_EMISSOR", nullable = false)
	private EmissorEntity emissor;
	
	@Column(name = "DESCRICAO", nullable = false)
	private String descricao;
	
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Column(name = "DATA_COMPRA")
	private String dataCompra;
	
	@Column(name =" VALOR")
	private Double valor;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	
	public LojaEntity getLoja() {
		return loja;
	}

	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}

	public EmissorEntity getEmissor() {
		return emissor;
	}

	public void setEmissor(EmissorEntity emissor) {
		this.emissor = emissor;
	}

	public String getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(String dataCompra) {
		this.dataCompra = dataCompra;
	}

	public CartaoEntity getCartao() {
		return cartao;
	}

	public void setCartao(CartaoEntity cartao) {
		this.cartao = cartao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	
}

//Lançamento (id_lancamento, id_cartao, id_loja, id_emissor, descricao, valor, data_compra)

