package br.com.dbccompany.Cartoes.Entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;





@Entity
@Table( name = "CREDENCIADOR_X_LOJA" )
public class Credenciador_X_LojaEntity {
	@SequenceGenerator( allocationSize = 1, name = "CREDENCIADOR_X_LOJA_SEQ", sequenceName  = "CREDENCIADOR_X_LOJA_SEQ")
	@Id
	@GeneratedValue( generator = "CREDENCIADOR_X_LOJA_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_CREDENCIADOR_X_LOJA",nullable = false)
	private Integer id;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable( name = "ID_LOJA", 
		joinColumns = {
			@JoinColumn( name = "ID_CREDENCIADOR_X_LOJA" )},
		inverseJoinColumns = {
			@JoinColumn( name = "ID_LOJA" )})
	private List<LojaEntity> loja = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable( name = "ID_CREDENCIADOR", 
		joinColumns = {
			@JoinColumn( name = "ID_CREDENCIADOR_X_LOJA" )},
		inverseJoinColumns = {
			@JoinColumn( name = "ID_CREDENCIADOR" )})
	private List<CredenciadorEntity> credenciador = new ArrayList<>();
	
	@Column(name = "TAXA")
	private Double taxa;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<LojaEntity> getLoja() {
		return loja;
	}

	public void setLoja(List<LojaEntity> loja) {
		this.loja = loja;
	}

	public List<CredenciadorEntity> getCredenciador() {
		return credenciador;
	}

	public void setCredenciador(List<CredenciadorEntity> credenciador) {
		this.credenciador = credenciador;
	}

	public Double getTaxa() {
		return taxa;
	}

	public void setTaxa(Double taxa) {
		this.taxa = taxa;
	}

	
	
	
}
