package br.com.dbccompany.Cartoes.Entity;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOJA")
public class LojaEntity {
	
	@Id
	@SequenceGenerator( allocationSize = 1, name = "LOJA_SEQ", sequenceName  = "LOJA_SEQ")
	@GeneratedValue( generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE )
	@Column( name = "ID_LOJA",nullable = false)
	private Integer id;
	
	@Column( name = "NOME")
	private String nome;

	@ManyToMany( mappedBy = "loja" )
	private List<Credenciador_X_LojaEntity> Credenciador_X_LojaEntity = new ArrayList<>();
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Credenciador_X_LojaEntity> getCredenciador_X_LojaEntity() {
		return Credenciador_X_LojaEntity;
	}

	public void setCredenciador_X_LojaEntity(List<Credenciador_X_LojaEntity> credenciador_X_LojaEntity) {
		Credenciador_X_LojaEntity = credenciador_X_LojaEntity;
	}

	
	
	
}
