
	Exercicio 1
	
	->db.createCollection("turismo")

	->db.turismo.insert({nome: "Charqueadas", fundacao: "28/03/1982", km_dePoa: 72.0, pontos_turisticos:[{nome: "Parcao", descricao:"Muita 		gente bebendo", entrada:0.0, horario:"Aberto 24h"},{nome: "Cais", descricao:"Um bom lugar parar ver o por do sol", entrada:0.0, 	horario: "Aberto 24h"}]})

	->db.turismo.insert({nome: "Arroio dos Ratos", fundacao: "12/04/1964", km_dePoa: 62.0, pontos_turisticos:[{nome: "Praca", 		descricao:"Lugar para levar as crianças", entrada:0.0, horario:"Aberto 24h"},{nome: "Arroio", descricao:"Lugar para tomar banho e 		adquirir doenças", entrada:0.0, horario: "Aberto 24h"}]})


	->db.turismo.insert([{nome: "Caxias do sul", fundacao: "20/06/1890", km_dePoa: 120.0, pontos_turisticos:[{nome: "Jardim Zoológico da 		UCS", descricao:"Zoologico e atividades ao ar livre", entrada:10.0, horario:"8h - 17h"},{nome: "Castelo Lacave", descricao:"Castelo 		estilo medieval", entrada:0.0, horario: "9h - 19h"}]},{nome: "Santa Cruz do Sul", fundacao: "31/03/1877", km_dePoa:155, 		pontos_turisticos:[{nome:"Parque da Gruta", descricao:"natureza", entrada: 0.0, horario:"8h - 18"},{nome:"Gruta dos Índios", 		descricao:"Indios" , entrada:1.0 , horario:"8h - 18h" }] } ])

	->db.turismo.insert([{nome: "Rio de Janeiro", fundacao: "01/03/1565", km_dePoa: 1569, pontos_turisticos:[{nome: "Cristo Redentor", 		descricao:"Estatua gigante de cristo", entrada:77.0, horario:"8h - 18h"},{nome: "Pao de Açucar", descricao:"Morro com vista pro mar", 		entrada:99, horario: "9h - 20h"},{nome:"Copacabana", descricao:"Praia famosa", entrada:0, horario:"Aberto 24" }]},{nome: "São Paulo", 		fundacao: "25/01/1554", km_dePoa:1143, pontos_turisticos:[{nome:"Museu de Arte de São Paulo", descricao:"Artes", entrada: 0.0, 		horario:"10h - 20h"},{nome:"Parque Ibirapuera", descricao:"Natureza, atividades ao ar livre" , entrada:0 , horario:"Aberto 24h" }] } ])

	
	Exercicio 2

	->db.turismo.updateOne({nome:"Charqueadas"},{$set:{populacao:40000}})
	
	->db.turismo.update({"nome":"São Paulo" },{ $unset: {"km_dePoa": ""}})

	->db.turismo.updateOne({nome:"Charqueadas"},{$set:{km_dePoa:75}})

	Exercicio 3
	
	db.turismo.remove({"_id" : ObjectId("5e43580b8f525496617b0173")},1)

	db.turismo.find({$and:[{"pontos_turisticos.entrada":10}, {"km_dePoa":{$gte:72}}]}).pretty()

	

	
	
