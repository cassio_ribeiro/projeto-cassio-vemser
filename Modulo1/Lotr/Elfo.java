

public class Elfo extends Personagem{
    private int indiceFlecha;
    private static int qtdElfos;
    
    {
        indiceFlecha = 0;
        qtdElfos = 0;
    }
    
    public Elfo ( String nome ){
       super(nome);
       this.vida = 100.0;
       this.inventario.adicionaItem( new Item(2, "Flecha") );
       this.inventario.adicionaItem( new Item(1, "Arco") );
       Elfo.qtdElfos++;
    }
    
    protected void finalize() throws Throwable {
        Elfo.qtdElfos--;
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    public Item getFlecha(){
        return this.inventario.obterItem(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
   
    
    public boolean podeAtirarFleca(){
        return this.getQtdFlecha() > 0;
    }
    public void atirarFlecha( Dwarf anao ){
       if(podeAtirarFleca()){
           this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
           this.perdeVida();
           this.aumentarXp();
           anao.perdeVida();
       }
       
    }
     public String imprimeResumo(){
        return "elfo";
    }
}
