

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoTest{
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Pé de ferro");
        novoElfo.atirarFlecha(anao);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        //assertEquals(100.0, anao.getVida());
        
    }
    
     @Test
    public void atirar2FlechasDiminuirZerarFlechasAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Pé de ferro");
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
        //assertEquals(90.0, anao.getVida());
        
    }
    
    @Test
    public void atirar3FlechasDiminuirZerarFlechasNaoAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Pé de ferro");
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        novoElfo.atirarFlecha(anao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
        //assertEquals(90.0, anao.getVida());
        
    }
    
    @Test
    public void elfoNasceComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
}
