

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest {
    
    @Test
    public void dwarfNasceCom110Devida() {
        Dwarf anao = new Dwarf("Pe de ferro");
        assertEquals(110.0, anao.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfPerde10DeVida(){
        Dwarf anao = new Dwarf("Pe de ferro");
        anao.perdeVida();
        assertEquals(100.0, anao.getVida(), 1e-9);
    }
   
    
    @Test 
    public void dwaarfPerdeTodaVida11Ataques(){
       Dwarf anao = new Dwarf("Pe de ferro");
       
       for(int i = 0; i < 11; i++){
           anao.perdeVida();
        }
        
        assertEquals(0.0, anao.getVida(), 1e-9);
    }
    
    @Test 
    public void dwaarfPerdeTodaVida12Ataques(){
       Dwarf anao = new Dwarf("Pe de ferro");
       
       for(int i = 0; i < 12; i++){
           anao.perdeVida();
        }
        
        assertEquals(0.0, anao.getVida(), 1e-9);
    }
    
    @Test 
    public void verificaStatusDepoisDePerdeTodaVida(){
        Dwarf anao = new Dwarf("Pe de ferro");
        
        for(int i = 0; i < 12; i++){
           anao.perdeVida();
        }
        
        assertEquals(Status.MORTO, anao.getStatus());
        
    }
    
    @Test 
    public void verificaSePerdeVidaDepoisDeMorto(){
        Dwarf anao = new Dwarf("Pe de ferro");
        
        for(int i = 0; i < 12; i++){
           anao.perdeVida();
        }
        
        assertEquals(0.0, anao.getVida(), 1e-9);
    
    }
    
    
    
    
}
