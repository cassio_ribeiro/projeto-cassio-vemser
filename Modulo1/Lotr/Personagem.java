

public abstract class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected double dano;
    protected int experiencia, qtdExperienciaPorAtaque;
    
    {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario();
        experiencia = 0;
        qtdExperienciaPorAtaque = 1;
        dano = 0.0;
    }
    
    
    public Personagem( String nome ){
        this.nome = nome;
    }
    
    public String getNome(){
        return this.nome;
    }
    
     public void setNome( String nome ){
        this.nome = nome;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    public double getVida(){
        return this.vida;
    } 
    
    public int getExperiencia(){
        return this.experiencia;
    }
    
    public boolean podePerder(){
        return this.getVida() > 0;
    }
    
    public void ganharItem(Item item){
        this.inventario.adicionaItem(item);
    }
    
    public void perderItem(Item item){
        this.inventario.removeItem(item);
    }
    
     public void aumentarXp(){
        experiencia = experiencia + qtdExperienciaPorAtaque;
    }
    
    public void perdeVida(){
            if( this.podePerder() && dano > 0.0){
                //comparacao ? verdadeiro : false
                this.vida = this.vida >= this.dano ? 
                this.vida - this.dano : 0.0;
                if ( this.vida == 0 ){
                    this.status = Status.MORTO;
                }else{
                    this.status = Status.SOFREU_DANO;
                }       
            }
        
    }
    
    public abstract String imprimeResumo();
        
    
    
    
    
}
