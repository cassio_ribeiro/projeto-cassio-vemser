

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest {
   
   
    @Test 
    public void testaSeElfoVerdeTemFlechaComoPrimeiroItem(){
        ElfoVerde elfoVerde = new ElfoVerde("Green Legolas");
        Item flecha = new Item (2, "Flecha");
        
        assertTrue(elfoVerde.getInventario().obterItem(0).getDescricao().equals(flecha.getDescricao()));
    }
    
    @Test 
    public void testaSeElfoVerdeNaoGanhaItensFora(){
        ElfoVerde elfoVerde = new ElfoVerde("Green Legolas");
        Item flecha = new Item (2, "Flecha");
        elfoVerde.ganharItem(flecha);
        assertNull(elfoVerde.getInventario().obterItem(2));
    }
    
    @Test 
    public void testaSeElfoVerdeGanhaItemSeu(){
        ElfoVerde elfoVerde = new ElfoVerde("Green Legolas");
        Item flecha = new Item (2, "Flecha de Vidro");
        elfoVerde.ganharItem(flecha);
        assertEquals(flecha,elfoVerde.getInventario().obterItem(2));
    }
    
    @Test 
    public void testaSeElfoVerdePerdeItemNaoSeu(){
        ElfoVerde elfoVerde = new ElfoVerde("Green Legolas");
        Item flecha = new Item (2, "Flecha");
        Item flechaVidro = new Item (2, "Flecha de Vidro");
        elfoVerde.ganharItem(flechaVidro);
        elfoVerde.perderItem(flecha);
        assertEquals(flechaVidro,elfoVerde.getInventario().obterItem(2));
    }
    
    @Test 
    public void testaSeElfoVerdePerdeItemSeu(){
        ElfoVerde elfoVerde = new ElfoVerde("Green Legolas");
        Item flecha = new Item (2, "Flecha");
        Item flechaVidro = new Item (2, "Flecha de Vidro");
        elfoVerde.ganharItem(flechaVidro);
        elfoVerde.perderItem(flechaVidro);
        assertNull(elfoVerde.getInventario().obterItem(2));
    }
    
    @Test 
    public void testeSeGanhaDobroDeXp(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        Dwarf anao = new Dwarf("Pé de ferro");
        novoElfo.atirarFlecha(anao);
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
    }
    
    
    
}
