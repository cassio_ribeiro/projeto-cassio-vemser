import java.util.*;

public class Inventario {
   
    
   private ArrayList<Item> itens;
    
   public Inventario(){
       itens =  new ArrayList<>();
   } 
   
   
   public void adicionaItem( Item item ){
       itens.add(item);
   }  
   
   public Item obterItem( int pos ){
       if( pos >= this.itens.size() ){
           return null;
       }
        return this.itens.get(pos);
   }
   
   public void removeItem( Item item ) {
       this.itens.remove(item);
   }
   
  public String getDescricoesItens(){
      StringBuilder descricoes = new StringBuilder();
      for( int i = 0; i < this.itens.size(); i++ ){
          Item item = itens.get(i);
          if( item != null ){
              descricoes.append(this.itens.get(i).getDescricao());
              descricoes.append(",");
          }
      }
      
      return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length ()- 1)) : descricoes.toString());
  }
  
  public ArrayList<Item> getItens(){
      return this.itens;
  }
  
  public Item maiorQtdItem(){
      Item maiorItem = new Item();
      maiorItem = this.itens.get(0);
      
      for( int i = 0; i < this.itens.size(); i++ ){
          if( this.itens.get(i).getQuantidade() > maiorItem.getQuantidade() ){
              maiorItem = itens.get(i);
          }
          
      }
      
      //return maiorItem;
      return this.itens.size() > 0 ? maiorItem : null; 
  }
    
  public Item buscar( String desc){
      
      for ( int i = 0; i < itens.size(); i++ ){
          if( desc.equals(itens.get(i).getDescricao())){
              return itens.get(i);
          }
      }
      return null;
  }
   
  public ArrayList<Item> inverte(){
      ArrayList<Item> itensInvertido = new ArrayList<>();
      
      for( int i = this.itens.size()-1; i >= 0; i -- ){
          itensInvertido.add(this.itens.get(i));
         
      }

      return itensInvertido;
      
  }
  
  public int tamanho(){
      return itens.size();
  }
  
  public void ordenarItens(){
      this.ordenarItens(TipoOrdenacao.ASC);
  }
  
  public void ordenarItens(TipoOrdenacao ordenacao){
      for( int i = 0; i < this.itens.size(); i++ ){
          for( int j = 0; j < this.itens.size() - 1; j++){
              Item atual = this.itens.get(j);
              Item proximo = this.itens.get(j + 1);
              boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? 
              atual.getQuantidade() > proximo.getQuantidade() :
              atual.getQuantidade() < proximo.getQuantidade();
              
              if( deveTrocar ){
                  Item itemTrocado = atual;
                  this.itens.set(j, proximo);
                  this.itens.set(j + 1, itemTrocado);
              }
          }
      }
  }
  
  public Inventario unirInventarios( Inventario inventarioAux ){
      Inventario inventarioRetorno = new Inventario();
      
      for( int i = 0; i < this.itens.size(); i++){
          inventarioRetorno.adicionaItem( this.itens.get(i));
      }
      
      for( int i = 0; i < inventarioAux.tamanho(); i++){
          inventarioRetorno.adicionaItem(inventarioAux.getItens().get(i));
      }
      
      return inventarioRetorno;
  }
  
  public Inventario diferenciarInventarios( Inventario inventarioAux ){
      //NAO ESTA FUNIONANDO
      Inventario inventarioRetorno = new Inventario();
      String stringItens = inventarioAux.getDescricoesItens();
      for( int i = 0; i < this.itens.size(); i++){
          Item itemAtual = this.itens.get(i);
          if(!itemAtual.getDescricao().contains(stringItens)){
              inventarioRetorno.adicionaItem( this.itens.get(i));
          }
      } 
      
      return inventarioRetorno;
  }
   

}
