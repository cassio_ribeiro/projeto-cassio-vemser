

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PaginadorInventarioTest {
    
    @Test
    public void testaMarcadorELimitador(){
        Inventario inventario = new Inventario();
        Item espada = new Item(5, "Espada");
        Item arco = new Item(6, "Arco");
        Item flecha = new Item(5, "Flecha");
        Item escudo = new Item(5, "Escudo");
        inventario.adicionaItem(espada);
        inventario.adicionaItem(arco);
        inventario.adicionaItem(flecha);
        inventario.adicionaItem(escudo);
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        
        paginador.pular(2);
        
        assertEquals(escudo,paginador.limitar(2).get(0));
        
        
        
        
        
        
        
    }
    
}
