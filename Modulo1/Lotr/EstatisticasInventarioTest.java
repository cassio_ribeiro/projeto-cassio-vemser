

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInventarioTest {
    
    @Test 
    public void testaSeRetornaMediaComVirgula(){
     Inventario inventario = new Inventario();
     EstatisticasInventario estatic = new EstatisticasInventario(inventario); 
     Item escudo = new Item(2, "escudo");
     Item espada = new Item(3, "espada");
     Item flecha = new Item(4, "flecha");
     inventario.adicionaItem(escudo);
     inventario.adicionaItem(espada);
     inventario.adicionaItem(flecha);
 
     assertEquals(3.0, estatic.calcularMedia(), 1e-9); 
        
    }
    
    @Test 
    public void testaSeRetornaMediaSemVirgula(){
     Inventario inventario = new Inventario();
     EstatisticasInventario estatic = new EstatisticasInventario(inventario); 
     Item escudo = new Item(2, "escudo");
     Item espada = new Item(3, "espada");
     Item flecha = new Item(4, "flecha");
     inventario.adicionaItem(escudo);
     inventario.adicionaItem(espada);
     inventario.adicionaItem(flecha);
 
     assertEquals(3, estatic.calcularMedia(), 1e-9); 
        
    }
    
    @Test 
    public void testaMediana(){
       Inventario inventario = new Inventario();
       Item chapeu = new Item(6, "chapeu");
       Item escudo = new Item(5, "escudo");
       Item espada = new Item(4, "espada");
       Item flecha = new Item(3, "flecha");
       Item flecha2 = new Item(2, "flecha2");
       Item lanca = new Item(1, "lanca");
       
       inventario.adicionaItem(chapeu);
       inventario.adicionaItem(escudo);
       inventario.adicionaItem(espada);
       inventario.adicionaItem(flecha);
       inventario.adicionaItem(flecha2);
       inventario.adicionaItem(lanca);
       
       EstatisticasInventario estatic = new EstatisticasInventario(inventario);
       
       assertEquals(3.5, estatic.calcularMediana(), 1e-9);
       
       
    } 
    
    
    @Test 
    public void testeAcimaDaMedia(){
       Inventario inventario = new Inventario();
       Item chapeu = new Item(6, "chapeu");
       Item escudo = new Item(5, "escudo");
       Item espada = new Item(4, "espada");
       Item flecha = new Item(3, "flecha");
       Item flecha2 = new Item(2, "flecha2");
       Item lanca = new Item(1, "lanca");
       
       inventario.adicionaItem(chapeu);
       inventario.adicionaItem(escudo);
       inventario.adicionaItem(espada);
       inventario.adicionaItem(flecha);
       inventario.adicionaItem(flecha2);
       inventario.adicionaItem(lanca);
       
       EstatisticasInventario estatic = new EstatisticasInventario(inventario);
       
       assertEquals(3, estatic.qtdItensAcimaDaMedia());
       
       
    }
    
}
