

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ExercitoTest {
    
    @Test
    public void testeSeAlistaSomenteElfosQuePodem(){
        Exercito arm = new Exercito();
        ElfoVerde elfoVerde = new ElfoVerde("green");
        ElfoNoturno elfoNoturno = new ElfoNoturno("black");
        ElfoDaLuz elfoBranco = new ElfoDaLuz("white");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("black");
        arm.alistar(elfoVerde);
        arm.alistar(elfoNoturno);
        arm.alistar( elfoBranco);
        arm.alistar(elfoNoturno2);
        
        assertEquals(elfoVerde, arm.buscarElfo(0));
        assertEquals(elfoNoturno, arm.buscarElfo(1));
         assertEquals(elfoNoturno2, arm.buscarElfo(2));
        //assertEquals(null,arm.buscarElfo(2));
    
    }
    
    @Test
    public void testeSeRetornaElfosComStatus(){
        Exercito arm = new Exercito();
        
        ElfoVerde elfoVerde = new ElfoVerde("green");
        ElfoNoturno elfoNoturno = new ElfoNoturno("black");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("black");
        
        arm.alistar(elfoVerde);
        arm.alistar(elfoNoturno);
        arm.alistar(elfoNoturno2);
        
        assertEquals(elfoVerde, arm.buscarStatus(Status.RECEM_CRIADO).get(0));
       
    }
    
    @Test
    public void testeSeRetornaStatusSofreuDano(){
        Exercito arm = new Exercito();
        
        ElfoVerde elfoNoturno = new ElfoVerde("Noturno");
       
        arm.alistar(elfoNoturno );
        elfoNoturno.atirarFlecha( new Dwarf("anao"));
        
        assertEquals(elfoNoturno , arm.buscarStatus(Status.SOFREU_DANO).get(0));
        
    
   
    }
}
