

public class Item{
    
    protected int quantidade;
    private String descricao;
   
    
    public Item( int quant, String desc ){
        this.quantidade = quant;
        this.descricao = desc;
   
    }
    
    public Item(){
    
    }
    public int getQuantidade(){
        return this.quantidade;
    }
    
    public void setQuantidade( int quant ){
        this.quantidade = quant;
    }
    
    public String getDescricao(){
        return this.descricao;
    }
    
    public void setDescricao( String desc ){
        this.descricao = desc;
    }
    
    public boolean equals(Object obj){
        Item outroItem = (Item)obj;
        return this.quantidade == outroItem.getQuantidade() && 
        this.descricao.equals(outroItem.getDescricao());
    }
    
    
}
