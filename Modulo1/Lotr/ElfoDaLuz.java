import java.util.*;

public class ElfoDaLuz extends Elfo{
    int ataque;
    private final double QTD_VIDA_GANHA = 10;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    {
        ataque = 0;
    
    }
    public ElfoDaLuz ( String nome ){
       super(nome);
       this.vida = 100.0;
       super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
       //this.qtdExperienciaPorAtaque = 10; 
       this.dano = 21.0;
    }
    
    @Override
    public void perderItem(Item item){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
    }
    
    public void ganharVida(){
        this.vida += QTD_VIDA_GANHA;
    }
    
    private boolean devePerderVida(){
        return ataque % 2 == 1;
    }
    
    public void atacarComEspada( Dwarf anao ) {
        if(this.getStatus() != Status.MORTO){
            anao.perdeVida();
            this.ataque++;
            this.aumentarXp();
            
            if( this.devePerderVida() ) {
                this.perdeVida();
            }else{
                ganharVida();
            }
        }
            
       
    }
   
}
