
import java.util.*;

public class Exercito {
    
    ArrayList<Elfo> elfos = new ArrayList<>();
    
    public Exercito(){
    
    }
    
    public void alistar(Elfo elfo){
        if(elfo instanceof ElfoVerde || elfo instanceof ElfoNoturno){
            this.elfos.add(elfo);
        }
    }
    
    public ArrayList<Elfo> buscarStatus(Status status){
        ArrayList<Elfo> elfosComStatus = new ArrayList<>();
        
        for( int i = 0; i < this.elfos.size(); i++ ){
            if( elfos.get(i).getStatus() == status ){
                elfosComStatus.add(elfos.get(i));
            }
        }
        
        
        return elfosComStatus;
    }
    
    public Elfo buscarElfo(int pos){
        return this.elfos.get(pos);
    }
    
}
