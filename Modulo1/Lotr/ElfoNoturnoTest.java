

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest {
    
    @Test
    public void testaSePerde15DeXp(){
        ElfoNoturno elfo = new ElfoNoturno("noturno");
        Dwarf anao = new Dwarf("pe de ferro");
        elfo.atirarFlecha( anao );
        assertEquals(85, elfo.getVida(), 1e-9);
    } 
    
    @Test
    public void testaSeXp(){
        ElfoNoturno elfo = new ElfoNoturno("noturno");
        Dwarf anao = new Dwarf("pe de ferro");
        elfo.atirarFlecha( anao );
        assertEquals(3, elfo.getExperiencia());
    } 
    
}
