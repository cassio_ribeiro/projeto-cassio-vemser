

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoDaLuzTest {
    
    @Test
    public void testeDeAtaques(){
       ElfoDaLuz elfo = new ElfoDaLuz("White Legolas");
       Dwarf anao = new Dwarf("LittleMan");
        
       elfo.atacarComEspada(anao);
       assertEquals(79.0, elfo.getVida(), 1e-9);
       
       elfo.atacarComEspada(anao);
       assertEquals(89.0, elfo.getVida(), 1e-9);
       
       elfo.atacarComEspada(anao);
       assertEquals(68.0, elfo.getVida(), 1e-9);
    
    }
    
}
