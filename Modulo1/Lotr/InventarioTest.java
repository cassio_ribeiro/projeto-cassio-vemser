

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class InventarioTest {
   @Test
   public void criaInventarioSemQuantidadeInformada(){
       Inventario inventario = new Inventario();
       assertEquals(0, inventario.getItens().size());
       
   }
   
   
   
   @Test
   public void adicionarUmItem() {
       Inventario inventario = new Inventario();
       Item espada = new Item(1, "Espada");
       inventario.adicionaItem(espada);
       assertEquals(espada, inventario.getItens().get(0));
   }
   
   @Test
   public void adicionardoisItens() {
       Inventario inventario = new Inventario();
       Item espada = new Item(1, "Espada");
       Item escudo = new Item(1, "escudo");
       inventario.adicionaItem(espada);
       inventario.adicionaItem(escudo);
       assertEquals(espada, inventario.getItens().get(0));
       assertEquals(escudo, inventario.getItens().get(1));
   }
   
   @Test
   public void obterItem() {
       Inventario inventario = new Inventario();
       Item espada = new Item(1, "Espada");
       inventario.adicionaItem(espada);
       assertEquals(espada, inventario.obterItem(0));
   }
   
   @Test
   public void excluirItem() {
       Inventario inventario = new Inventario();
       Item espada = new Item(1, "Espada");
       Item escudo = new Item(1, "escudo");
       inventario.adicionaItem(espada);
       inventario.adicionaItem(escudo);
       inventario.removeItem(espada);
       assertEquals(escudo,inventario.obterItem(0));
   }
   
   @Test 
   public void buscarItemPorDescricao(){
       Inventario inventario = new Inventario();
       Item espada = new Item(1, "espada");
       Item escudo = new Item(1, "escudo");
       inventario.adicionaItem(espada);
       inventario.adicionaItem(escudo);
       
       inventario.removeItem(escudo);
       assertEquals(espada,inventario.buscar("espada"));
    
   }
   @Test 
   public void buscarItemComMesmaDescricao(){
       Inventario inventario = new Inventario();
       Item espada = new Item(1, "espada");
       Item escudo = new Item(1, "espada");
       inventario.adicionaItem(espada);
       inventario.adicionaItem(escudo);
       
       
       assertEquals(espada,inventario.buscar("espada"));
    
   }
   
   @Test 
   public void verSeInverteu(){
       Inventario inventario = new Inventario(); 
      
       Item espada = new Item(1, "espada");
       Item escudo = new Item(1, "escudo");
       
       inventario.adicionaItem(espada);
       inventario.adicionaItem(escudo);
       
       assertEquals(inventario.obterItem(0), inventario.inverte().get(1));
       assertEquals(inventario.obterItem(1), inventario.inverte().get(0));
       assertEquals(2, inventario.inverte().size());
   }
   
   @Test
   public void inverterInventarioVazio(){
       Inventario inventario = new Inventario();
       assertTrue(inventario.inverte().isEmpty());
       
   }
   
   @Test
   public void inverterInventarioComUmItem(){
       Inventario inventario = new Inventario();
       Item arco = new Item(1, "Arco");
       inventario.adicionaItem(arco);
       assertEquals(arco, inventario.inverte().get(0));
       
   }
   
   @Test
   public void getDescricoesVariosItens(){
       Inventario inventario = new Inventario();
       Item espada = new Item(1, "Espada");
       Item lanca = new Item(1, "Lança");
       Item escudo = new Item(1, "Escudo");
       
       inventario.adicionaItem(espada);
       inventario.adicionaItem(lanca);
       inventario.adicionaItem(escudo);
       
       assertEquals("Espada,Lança,Escudo", inventario.getDescricoesItens());
       
   }
   
   @Test
   public void ItemComMaiorQuantidade(){
       Inventario inventario = new Inventario();
       Item espada = new Item(2, "Espada");
       Item lanca = new Item(1, "Lança");
       Item escudo = new Item(1, "Escudo");
       
       inventario.adicionaItem(espada);
       inventario.adicionaItem(lanca);
       inventario.adicionaItem(escudo);
       
       assertEquals(espada, inventario.maiorQtdItem());
       
   }
   
   @Test 
   public void testeUnirInventarios(){
       Inventario inventario = new Inventario();
       Item espada = new Item(2, "Espada");
       Item lanca = new Item(1, "Lança");
       Item escudo = new Item(1, "Escudo");
       inventario.adicionaItem(espada);
       inventario.adicionaItem(lanca);
       inventario.adicionaItem(escudo);
       
       Inventario inventarioDois = new Inventario();
       Item espadaDois = new Item(2, "EspadaDois");
       Item lancaDois = new Item(1, "LançaDois");
       Item escudoDois = new Item(1, "EscudoDois");
       
       inventarioDois.adicionaItem(espadaDois);
       inventarioDois.adicionaItem(lancaDois);
       inventarioDois.adicionaItem(escudoDois);
       
    
       assertEquals(lancaDois, inventario.unirInventarios(inventarioDois).obterItem(4));
       
       
   }
   
   @Test 
   public void testeUnirInventariosDois(){
       Inventario inventario = new Inventario();
       Item espada = new Item(2, "Espada");
       Item lanca = new Item(1, "Lança");
       Item escudo = new Item(1, "Escudo");
       inventario.adicionaItem(espada);
       inventario.adicionaItem(lanca);
       inventario.adicionaItem(escudo);
       
       Inventario inventarioDois = new Inventario();
       Item espadaDois = new Item(2, "EspadaDois");
       Item lancaDois = new Item(1, "LançaDois");
       Item escudoDois = new Item(1, "EscudoDois");
       
       inventarioDois.adicionaItem(espadaDois);
       inventarioDois.adicionaItem(lancaDois);
       inventarioDois.adicionaItem(escudoDois);
       
       assertEquals(escudoDois, inventario.unirInventarios(inventarioDois).obterItem(5));
       
       
   }
   
   @Test 
   public void seDiferenciouOsInventarios(){
       Inventario inventario = new Inventario();
       Item espada = new Item(2, "Espada");
       Item lanca = new Item(1, "Lança");
       Item escudo = new Item(1, "Escudo");
       inventario.adicionaItem(espada);
       inventario.adicionaItem(lanca);
       inventario.adicionaItem(escudo);
       
       Inventario inventarioDois = new Inventario();
       Item espadaDois = new Item(2, "Espada");
       Item lancaDois = new Item(1, "Lança");
       Item escudoDois = new Item(1, "EscudoDois");
       
       inventarioDois.adicionaItem(espadaDois);
       inventarioDois.adicionaItem(lancaDois);
       inventarioDois.adicionaItem(escudoDois);
       
       assertEquals(espadaDois, inventario.diferenciarInventarios(inventarioDois).obterItem(3));
       
       
   }
}
