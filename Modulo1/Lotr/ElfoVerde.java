
import java.util.*;

public class ElfoVerde extends Elfo{
    
    private final ArrayList<String> itensElfoVerde = new ArrayList<>(
        Arrays.asList(
           "Espada de aço valiriano",
           "Arco de Vidro",
           "Flecha de Vidro"
        )
    );
    
    
    public ElfoVerde( String nome ){
       super(nome);
       this.qtdExperienciaPorAtaque = 2;
    }
    
   
    @Override
    public void ganharItem(Item item){
        boolean descricaoValidas = itensElfoVerde.contains(item.getDescricao());
        if( descricaoValidas ){
            this.inventario.adicionaItem(item);
        }
    }
    
    @Override
    public void perderItem(Item item){
        boolean descricaoValidas = itensElfoVerde.contains(item.getDescricao());
        if( descricaoValidas ){
            this.inventario.removeItem(item);
        }
    }

}

