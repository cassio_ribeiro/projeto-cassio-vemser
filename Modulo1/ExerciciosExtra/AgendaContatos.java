import java.util.*;
import java.util.HashMap;

public class AgendaContatos {
    
    private HashMap <String, String> agenda;
    
    public AgendaContatos(){
        agenda = new LinkedHashMap<>();
    } 
    public void adicionaContato( String nome, String telefone){
        agenda.put(nome , telefone);
    }
    
    public String buscaTelefone( String nome ){
        return this.agenda.get(nome);
    }
    
    public String buscaContatoPorTelefone ( String telefone ){
        for(HashMap.Entry<String, String> par : agenda.entrySet()){
            if( par.getValue().equals(telefone) ) {
                return par.getKey();
            }
        }
        return null;
    } 
    
    public String csv(){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        
        for (HashMap.Entry<String, String> par : agenda.entrySet()){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s, %s%s", chave, valor, separador);
            builder.append(agenda);
        }
        
        return builder.toString();
       
       
    }
    
    
    
    
    
    
}
